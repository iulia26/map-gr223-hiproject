module socialnetwork{
    requires java.sql;
    exports ro.ubbcluj.map.model.validators;
    exports ro.ubbcluj.map.model;
    exports ro.ubbcluj.map.myException;
    exports ro.ubbcluj.map.repository.dbo;
    exports ro.ubbcluj.map.repository.paging;
    exports ro.ubbcluj.map.repository;
    exports ro.ubbcluj.map.Service;
    exports ro.ubbcluj.map.ui;
    exports ro.ubbcluj.model.validators;
    exports ro.ubbcluj.map.utils.events;
    exports ro.ubbcluj.map.utils.observer;
}