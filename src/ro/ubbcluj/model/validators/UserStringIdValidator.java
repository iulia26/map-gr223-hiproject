package ro.ubbcluj.model.validators;
import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.model.validators.Validator;

public class UserStringIdValidator implements Validator<ApplicationUser> {

    @Override
    public void validate(ApplicationUser entity) throws ValidationException {
                //sa validez daca in adresa de e-mail are arond si cel putin un punct
        if(entity.getId()==null || !entity.getId().matches("^.+@.+\\.[a-zA-Z]+$"))
            throw  new ValidationException("Invalid user id\n");
        if(entity==null || entity.getFirstName().isEmpty())
            throw new ValidationException("Invalid first name\n");
        if(entity==null || entity.getLastName().isEmpty())
            throw new ValidationException("Invalid last name\n");
    }

}
