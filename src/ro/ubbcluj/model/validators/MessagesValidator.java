package ro.ubbcluj.model.validators;

import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.Message;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.model.validators.Validator;

import java.util.Objects;

public class MessagesValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        String errors="";
        if(Objects.equals(entity.getMessage(), ""))
            errors+="Invalid message!\n";
        if(entity.getFrom().getId().equals(""))
            errors+="Invalid sender!\n";
        for(ApplicationUser user:entity.getTo())
        {if(user.getId().equals(""))
            errors+="Invalid user!\n";
            }
        if(entity.getDate()==null)
            errors+="Invalid dateTime\n";
        if(entity.getId()==null)
            errors+="Invalid Id\n";
    }
}
