package ro.ubbcluj.model.validators;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.model.validators.Validator;

public class FriendshipTupleIdValidator implements Validator<Friendship> {
        @Override
        public void validate(Friendship friendship) throws ValidationException {

            if(friendship.getUser1ID()==null ||friendship.getUser1ID().isEmpty() || !friendship.getUser1ID().matches("^.+@.+\\.[a-zA-Z]+$") )
                throw new ValidationException("User1 id doesn't match the pattern\n");
            if(friendship.getUser2ID()==null || friendship.getUser2ID().isEmpty() || !friendship.getUser2ID().matches("^.+@.+\\.[a-zA-Z]+$"))
                throw new ValidationException("User2 id doesn't match the pattern\n");
            if(friendship.getId().isEmpty())
                throw new ValidationException("Friendship id can't be empty");
        }
}

