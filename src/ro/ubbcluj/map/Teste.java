package ro.ubbcluj.map;

import ro.ubbcluj.map.Service.FriendshipService;
import ro.ubbcluj.map.Service.NetworkService;
import ro.ubbcluj.map.Service.UserService;
import ro.ubbcluj.map.model.*;
import ro.ubbcluj.map.myException.IDisAlreadyTakenException;
import ro.ubbcluj.map.myException.InsufficientDataToExecuteTaskException;
import ro.ubbcluj.map.myException.RepoError;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.dbo.FriendshipRepoDbo;
import ro.ubbcluj.map.repository.dbo.UserRepoDbo;
import ro.ubbcluj.model.validators.FriendshipTupleIdValidator;
import ro.ubbcluj.model.validators.UserStringIdValidator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.StreamSupport;

public class Teste {

    private void cleanupUsersDataBase(String id){
        String url="jdbc:postgresql://localhost:5432/SocialNetworkDB";
        String username="postgres";
        String password="mateinfo24";
        String sql="DELETE from application_users  where user_id=?";
        try(Connection connection= DriverManager.getConnection(url,username,password);
            PreparedStatement ps=connection.prepareStatement(sql))   {
            ps.setString(1,id);
            ps.executeUpdate();

        }catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void cleanupFriendshipsDataBase(String id1,String id2){
        String url="jdbc:postgresql://localhost:5432/SocialNetworkDB";
        String username="postgres";
        String password="mateinfo24";
        String sql="DELETE from friendships  where (user1_id,user2_id) in ((?,?),(?,?))";
        try(Connection connection= DriverManager.getConnection(url,username,password);
            PreparedStatement ps=connection.prepareStatement(sql))   {
            ps.setString(1,id1);
            ps.setString(2,id2);
            ps.setString(3,id2);
            ps.setString(4,id1);
            ps.executeUpdate();

        }catch (SQLException e) {
           e.printStackTrace();
        }

    }
    private void testNetworkService0(NetworkService networkService){
        int initCommunities=networkService.communitiesCounter();
        int usersBeforeAddOperation=networkService.getAllUsers().size();
       /* try {
            assert(networkService.addUser("a","a","a@test.ro","")==null);
            assert (networkService.addUser("b","b","b@test.ro","")==null);
            assert (networkService.addUser("c","c","c@test.ro","")==null);
            assert (networkService.addUser("d","d","d@test.ro","")==null);
            assert (networkService.addUser("e","e","e@test.ro","")==null);
            assert (networkService.addUser("f","f","f@test.ro","")==null);

        } catch (IDisAlreadyTakenException e) {
            assert false;
        }*/
        assert networkService.getAllUsers().size()==usersBeforeAddOperation+6;
        try {//formez inca 2 comunitati
            //prima comunitate
            assert(networkService.addFriendship("a@test.ro","b@test.ro"));
            assert (networkService.addFriendship("d@test.ro","a@test.ro"));
            assert (networkService.addFriendship("d@test.ro","e@test.ro"));
            //a doua comunitate
           assert( networkService.addFriendship("f@test.ro","c@test.ro"));
            //incerc sa adaug o prietenie formata din useri inexistenti

        } catch (InsufficientDataToExecuteTaskException e) {
            e.printStackTrace();
        }
        try{ networkService.addFriendship("NuExist@test.ro","c@test.ro");}catch(InsufficientDataToExecuteTaskException e){assert true;}//invalid users}
        assert (networkService.communitiesCounter()==initCommunities+2);//we have 2 connected components in the test file
        try {
           assert networkService.friendliestCommunity().size()>=3;
        } catch (InsufficientDataToExecuteTaskException a) {
            assert false;//we'll end up here only when there are no friendships formed yet
        }

        //testam functionalitatea de cautare a pritenilor dupa un id
        try{
            assert networkService.getFriendshipList("a@test.ro").size()==2;
            assert networkService.getFriendshipList("f@test.ro").size()==1;
            assert networkService.getFriendshipList("c@test.ro").size()==1;
            assert true;
        }catch (InsufficientDataToExecuteTaskException a){
            assert false;
        }catch (RepoError b){assert false;}

        //testam functionalitatea de cautare a pritenilor dupa un id si data
        try{
            assert networkService.getFriendshipListByDate("a@test.ro",LocalDate.MIN.getMonthValue()).size()==2;
            assert networkService.getFriendshipListByDate("f@test.ro",LocalDate.MIN.getMonthValue()).size()==1;
            assert networkService.getFriendshipListByDate("c@test.ro",LocalDate.MIN.getMonthValue()).size()==1;
            assert networkService.getFriendshipListByDate("a@test.ro",LocalDate.MIN.getMonthValue()).size()==2;
            assert networkService.getFriendshipListByDate("f@test.ro",LocalDate.MIN.getMonthValue()).size()==1;
            assert networkService.getFriendshipListByDate("c@test.ro",LocalDate.MIN.getMonthValue()).size()==1;

            assert true;
        }catch (InsufficientDataToExecuteTaskException a){
            assert false;
        }catch (RepoError b){assert false;}

        //Delete friendship
        assert(networkService.deleteFriendship("c@test.ro","f@test.ro")==null);
        assert(networkService.deleteFriendship("c@test.ro","f@test.ro")!=null);
        assert(networkService.deleteFriendship("f@test.ro","c@test.ro")!=null);
        assert(networkService.deleteFriendship("nuExist@test.ro","c@test.ro")!=null);//sterg o prietenie formata din useri inexistenti
       /* try{
            assert networkService.getFriendsList("f@test.ro").size()==0;
            assert networkService.getFriendsList("c@test.ro").size()==0;
            assert true;
        }catch (InsufficientDataToExecuteTaskException a){
            assert false;
        }catch (RepoError b){assert false;}


        //Delete user
        try{
            //vreau sa il sterg pe a
            assert (networkService.getFriendsList("d@test.ro").size()==2);
            assert (networkService.getFriendsList("b@test.ro").size()==1);
            networkService.deleteUser("a@test.ro");
            //a era prieten cu b si cu d
            assert (networkService.getFriendsList("d@test.ro").size()==1);
            assert (networkService.getFriendsList("b@test.ro").size()==0);
            //If user has ben
            //successfully erased than it'll be removed from the other users friend list
            assert true;
        }catch (InsufficientDataToExecuteTaskException e){
            assert false;
        }catch (RepoError e){assert false;}*/
        this.cleanupFriendshipsDataBase("a@test.ro","b@test.ro");
        this.cleanupFriendshipsDataBase("d@test.ro","e@test.ro");
        this.cleanupFriendshipsDataBase("a@test.ro","d@test.ro");
        this.cleanupFriendshipsDataBase("c@test.ro","f@test.ro");
        this.cleanupUsersDataBase("a@test.ro");
        this.cleanupUsersDataBase("b@test.ro");
        this.cleanupUsersDataBase("c@test.ro");
        this.cleanupUsersDataBase("d@test.ro");
        this.cleanupUsersDataBase("e@test.ro");
        this.cleanupUsersDataBase("f@test.ro");

    }
    private void testGraphUtilities(){
        //Graful de test1
        ArrayList<ArrayList<Integer>> adj1=new ArrayList<>();
        for(int i=0;i<7;i++)
            adj1.add(i,new ArrayList<Integer>());
        adj1.get(0).addAll(Arrays.asList(1,2,3,4));
        adj1.get(1).addAll(Arrays.asList(0,6));
        adj1.get(2).addAll(Arrays.asList(0,3));
        adj1.get(3).addAll(Arrays.asList(2,0,6,4));
        adj1.get(4).addAll(Arrays.asList(3,0,5));
        adj1.get(5).addAll(Arrays.asList(6,4));
        adj1.get(6).addAll(Arrays.asList(1,3,5));
        Graph g1=new Graph(adj1);
        assert(g1.longestPath()==6);
        //Graful de test2
        ArrayList<ArrayList<Integer>> adj2=new ArrayList<>();
        for(int i=0;i<5;i++)
            adj2.add(i,new ArrayList<Integer>());
        adj2.get(0).addAll(Arrays.asList(1));
        adj2.get(1).addAll(Arrays.asList(0,2));
        adj2.get(2).addAll(Arrays.asList(1,4,3));
        adj2.get(3).addAll(Arrays.asList(2));
        adj2.get(4).addAll(Arrays.asList(2));
        Graph g2=new Graph(adj2);
        assert(g2.longestPath()==3);
        //Graful de test3
        ArrayList<ArrayList<Integer>> adj3=new ArrayList<>();
        for(int i=0;i<5;i++)
            adj3.add(i,new ArrayList<Integer>());
        adj3.get(0).addAll(Arrays.asList(1));
        adj3.get(1).addAll(Arrays.asList(0,2));
        adj3.get(2).addAll(Arrays.asList(1,4,3));
        adj3.get(3).addAll(Arrays.asList(2,4));
        adj3.get(4).addAll(Arrays.asList(2,3));
        Graph g3=new Graph(adj3);
        assert(g3.longestPath()==4);
        //Graful de test4
        ArrayList<ArrayList<Integer>> adj4=new ArrayList<>();
        for(int i=0;i<3;i++)
            adj4.add(i,new ArrayList<Integer>());
        adj4.get(0).addAll(Arrays.asList(1,2));
        adj4.get(1).addAll(Arrays.asList(0,2));
        adj4.get(2).addAll(Arrays.asList(0,1));
        Graph g4=new Graph(adj4);
        assert(g4.longestPath()==2);
        //Graful de test5
        ArrayList<ArrayList<Integer>> adj5=new ArrayList<>();
        for(int i=0;i<3;i++)
            adj5.add(i,new ArrayList<Integer>());
        adj5.get(0).addAll(Arrays.asList(1));
        adj5.get(1).addAll(Arrays.asList(0));
        Graph g5=new Graph(adj5);
        assert(g5.longestPath()==1);
    }

    public void testRepoUserDBO(Repository<String, ApplicationUser>repoUser){
        long initSizeRepo= StreamSupport.stream(repoUser.findAll().spliterator(),false).count();
        long curentSize=initSizeRepo;
        ApplicationUser user1=new ApplicationUser("test1","test1*");user1.setId("t1@gmail.com");
        ApplicationUser user2=new ApplicationUser("test2","test2*");user2.setId("t2@gmail.com");
        ApplicationUser user3=new ApplicationUser("test3","test3*");user3.setId("t3@gmail.com");
        ApplicationUser user4=new ApplicationUser("test4","test4*");user4.setId("t4@gmail.com");
        ApplicationUser user5=new ApplicationUser("test5","test5*");user5.setId("t5@gmail.com");
        assert (repoUser.save(user1)==null);
        assert (repoUser.save(user2)==null);
        assert(repoUser.save(user3)==null);
        assert (repoUser.save(user4)==null);
        assert(repoUser.save(user5)==null);
        assert(StreamSupport.stream(repoUser.findAll().spliterator(),false).count()==initSizeRepo+5);
        assert (repoUser.save(user1)!=null);//Nu pot adauga acelasi user de mai multe ori
        assert (repoUser.save(user5)!=null);
        assert(StreamSupport.stream(repoUser.findAll().spliterator(),false).count()==initSizeRepo+5);
        ApplicationUser user6=repoUser.findOne("t2@gmail.com");//test FindOne
        assert (user6!=null);
        assert (user6.isValid() && user6.getId().equals("t2@gmail.com") && user6.getFirstName().equals("test2"));
       user2.setFirstName("t2NEW");
        assert (repoUser.update(user2)==null);
        user6=repoUser.findOne("t2@gmail.com");
        assert (user6.getFirstName().equals("t2NEW"));
        assert (repoUser.delete(user1)==null);
        assert (repoUser.delete(user1)!=null);//Can't double delete one user
        user1.setFirstName("t1NEW");
        user1.setLastName("t1NEW*");
        user1.setId("t1@gmail.com");
        assert(repoUser.findOne("t1@gmail.com")==null);
        //vreau sa introduc un alt utilizator cu acelasi id ca cel care a fost sters
        assert (repoUser.save(user1)==null);
        assert (repoUser.findOne(user1.getId()).getFirstName().equals("t1NEW"));
        assert(StreamSupport.stream(repoUser.findAll().spliterator(),false).count()==initSizeRepo+5);
        //numarul entitatilor din baza de date ar trebui sa fie acelasi deoarece aplic soft delete
        //sterg entitatile aduagate
        assert(repoUser.delete(user1)==null);
        assert(repoUser.delete(user2)==null);
        assert(repoUser.delete(user3)==null);
        assert(repoUser.delete(user4)==null);
        assert(repoUser.delete(user5)==null);

    }
    public void testRepoFriendshipDBO(Repository<Tuple<String,String>, Friendship> repoFriendship){
        long initSizeRepo= StreamSupport.stream(repoFriendship.findAll().spliterator(),false).count();
        Friendship frnds1=new Friendship("t1@gmail.com","t2@gmail.com", LocalDate.now());
        Friendship frnds2=new Friendship("t2@gmail.com","t3@gmail.com",LocalDate.now());
        Friendship frnds3=new Friendship("t5@gmail.com","t4@gmail.com",LocalDate.now());
        Friendship frnds4=new Friendship("t1@gmail.com","t5@gmail.com",LocalDate.now());
        assert (repoFriendship.save(frnds1)==null);
        assert (repoFriendship.save(frnds2)==null);
        assert (repoFriendship.save(frnds3)==null);
        assert (repoFriendship.save(frnds4)==null);
        assert (initSizeRepo+4==StreamSupport.stream(repoFriendship.findAll().spliterator(),false).count());
        //nu pot adauga o prietenie deja existenta
        assert (repoFriendship.save(frnds1)!=null);
        Friendship frnds5=new Friendship(frnds1.getUser2ID(), frnds1.getUser1ID());
        assert (repoFriendship.save(frnds5)!=null);//am inversat id-urile user-ilor pentru a verifica daca asa prietenia se mai adauga
        assert (initSizeRepo+4==StreamSupport.stream(repoFriendship.findAll().spliterator(),false).count());
        frnds5=repoFriendship.findOne(frnds3.getId());
        assert (frnds5.getUser1ID().equals("t4@gmail.com"));
        assert (frnds5.getUser2ID().equals("t5@gmail.com"));//retin id-urile in ordine rescatoare
        //Testez update

        assert (frnds5.getStartDate().equals(LocalDate.now()));
        frnds5.setStartDate(LocalDate.of(2002,3,2));
        assert (repoFriendship.update(frnds5)==null);
        frnds5=repoFriendship.findOne(frnds3.getId());
        assert (frnds5.getStartDate().equals(LocalDate.of(2002,3,2)));
        assert (repoFriendship.delete(frnds1)==null);
        assert (repoFriendship.delete(frnds2)==null);
        assert (repoFriendship.delete(frnds3)==null);
        assert (repoFriendship.delete(frnds4)==null);
        this.cleanupFriendshipsDataBase("t1@gmail.com","t2@gmail.com");
        this.cleanupFriendshipsDataBase("t2@gmail.com","t3@gmail.com");
        this.cleanupFriendshipsDataBase("t4@gmail.com","t5@gmail.com");
        this.cleanupFriendshipsDataBase("t1@gmail.com","t5@gmail.com");
        this.cleanupUsersDataBase("t1@gmail.com");
        this.cleanupUsersDataBase("t2@gmail.com");
        this.cleanupUsersDataBase("t3@gmail.com");
        this.cleanupUsersDataBase("t4@gmail.com");
        this.cleanupUsersDataBase("t5@gmail.com");

    }
    public void testDeleteFriendshipRequest(){


    }
    public void testAll(){



    }
}
