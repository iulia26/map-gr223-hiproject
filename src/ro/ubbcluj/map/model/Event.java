package ro.ubbcluj.map.model;

import java.time.LocalDate;
import java.util.List;

public class Event extends Entity<Long>{

    Long id;
    String description;
    LocalDate eventDate;
    List<ApplicationUser> subscribers;

    public Event(String description, LocalDate eventDate,List<ApplicationUser> subscribers,Long id) {
        this.description = description;
        this.eventDate = eventDate;
        this.subscribers = subscribers;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDate eventDate) {
        this.eventDate = eventDate;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public List<ApplicationUser> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<ApplicationUser> subscribers) {
        this.subscribers = subscribers;
    }

    @Override
    public String toString() {


        return id.toString() + " " + description + eventDate.toString();
    }
}
