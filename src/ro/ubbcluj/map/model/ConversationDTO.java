package ro.ubbcluj.map.model;

public class ConversationDTO {

    UserDto<String> user1;
    UserDto<String> user2;
    Long lastReadMessageID;
    Long conversationID = null;

    public ConversationDTO(UserDto<String> user1, UserDto<String> user2, Long lastReadMessageID) {
        this.user1 = user1;
        this.user2 = user2;
        this.lastReadMessageID = lastReadMessageID;
    }

    public ConversationDTO(UserDto<String> user1, UserDto<String> user2, Long lastReadMessageID, Long conversationID) {
        this.user1 = user1;
        this.user2 = user2;
        this.lastReadMessageID = lastReadMessageID;
        this.conversationID = conversationID;
    }

    public UserDto<String> getUser1() {
        return user1;
    }

    public void setUser1(UserDto<String> user1) {
        this.user1 = user1;
    }

    public UserDto<String> getUser2() {
        return user2;
    }

    public void setUser2(UserDto<String> user2) {
        this.user2 = user2;
    }

    public Long getLastReadMessageID() {
        return lastReadMessageID;
    }

    public void setLastReadMessageID(Long lastReadMessageID) {
        this.lastReadMessageID = lastReadMessageID;
    }

    public Long getConversationID() {
        return conversationID;
    }

    public void setConversationID(Long conversationID) {
        this.conversationID = conversationID;
    }
}
