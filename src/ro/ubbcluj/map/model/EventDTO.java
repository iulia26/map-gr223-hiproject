package ro.ubbcluj.map.model;

import java.time.LocalDate;
import java.util.List;

public class EventDTO {

    Long id;
    String description;
    LocalDate eventDate;
    List<UserDto<String>> subscribers;

    public EventDTO(Long id, String description, LocalDate eventDate, List<UserDto<String>> subscribers) {
        this.id = id;
        this.description = description;
        this.eventDate = eventDate;
        this.subscribers = subscribers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDate eventDate) {
        this.eventDate = eventDate;
    }

    public List<UserDto<String>> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<UserDto<String>> subscribers) {
        this.subscribers = subscribers;
    }

    @Override
    public String toString() {
        return this.description + " " + this.eventDate.toString() + " " + this.id.toString();
    }
}
