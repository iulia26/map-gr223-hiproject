package ro.ubbcluj.map.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @param <ID> id-ul entitatii
 */
public class Entity<ID> implements Serializable {

   // private static final long serialVersionUID = 7331115341259248461L;
    protected ID id;

    /**
     *
     * @return entity id
     */
    public ID getId() {
        return id;
    }

    /**
     *
     * @param id of type ID
     */
    public void setId(ID id) {
        this.id = id;
    }

    /**
     *
     * @param o Object
     * @return true if entity equals (E)Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;
        Entity<?> entity = (Entity<?>) o;
        return getId().equals(entity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    /**
     *
     * @return String representing the entity
     */
    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                '}';
    }

}