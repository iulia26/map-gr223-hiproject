package ro.ubbcluj.map.model;

import java.time.LocalDate;
import java.util.Date;

/**
 * Friendship extends entity<ID> where ID=Tuple<String,String>
 */
public class Friendship extends Entity<Tuple<String,String>> {

    String user1ID;
    String user2ID;
    Boolean isValid;
    LocalDate startDate;

    /**
     *
     * @param user1ID String
     * @param user2ID String
     * @param startDate LocalDate
     */
    public Friendship(String user1ID, String user2ID, LocalDate startDate) {
        this.user1ID = user1ID;
        this.user2ID = user2ID;
        this.isValid = true;
        this.startDate=startDate;
        super.setId(new Tuple<String ,String>(user1ID,user2ID));
    }

    public Friendship(String user1ID, String user2ID) {
        this.user1ID = user1ID;
        this.user2ID = user2ID;
        this.isValid = true;
        this.startDate=null;
        super.setId(new Tuple<String ,String>(user1ID,user2ID));
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getUser1ID() {
        return user1ID;
    }

    public void setUser1ID(String user1ID) {
        this.user1ID = user1ID;
    }

    public String getUser2ID() {
        return user2ID;
    }

    public void setUser2ID(String user2ID) {
        this.user2ID = user2ID;
    }

    public Boolean isValid() {
        return isValid;
    }

    public void setIsValid(Boolean valid) {
        isValid = valid;
    }

    @Override
    public Tuple<String, String> getId() {
        return super.getId();
    }

    @Override
    public void setId(Tuple<String, String> stringStringTuple) {
        super.setId(stringStringTuple);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
