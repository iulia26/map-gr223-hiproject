package ro.ubbcluj.map.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendshipRequest extends  Entity<Long>{

   ApplicationUser from;
   ApplicationUser to;
   FriendshipRequestStatus status;
   LocalDate date;

   public FriendshipRequest(ApplicationUser from, ApplicationUser to, Long id,LocalDate date) {
      this.from = from;
      this.to = to;
      this.status = FriendshipRequestStatus.PENDING;
      this.date=date;
      super.setId(id);
   }

   public ApplicationUser getFrom() {
      return from;
   }

   public void setFrom(ApplicationUser from) {
      this.from = from;
   }

   public ApplicationUser getTo() {
      return to;
   }

   public void setTo(ApplicationUser to) {
      this.to = to;
   }

   public FriendshipRequestStatus getStatus() {
      return status;
   }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setStatus(FriendshipRequestStatus status) {
      this.status = status;
   }

   @Override
   public String toString() {
      return "FriendshipRequest{" +
              "id=" + String.valueOf(id) +
              ", from=" + from.getId() +
              ", to=" + to.getId() +
              ", status=" + status +
              '}';
   }
}
