package ro.ubbcluj.map.model;

public enum SortingOrder {
    ASC,
    DESC
}
