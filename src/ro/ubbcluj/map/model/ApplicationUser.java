package ro.ubbcluj.map.model;

import java.util.ArrayList;
import java.util.List;

public class ApplicationUser extends Entity<String>{
    private String firstName,lastName;
    private List<ApplicationUser> friendsList;
    Boolean isValid;
    public List<ApplicationUser> getFriendsList() {
        return friendsList;
    }

    @Override
    public String toString(){
        return firstName+" "+
                lastName + ",e-mail:"+this.getId();
    }
    public ApplicationUser(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        friendsList=new ArrayList<ApplicationUser>();
        this.isValid=true;
    }

    public ApplicationUser(String firstName, String lastName,String id) {
        this.firstName = firstName;
        this.lastName = lastName;
        friendsList=new ArrayList<ApplicationUser>();
        this.isValid=true;
        this.setId(id);
    }
    public Boolean isValid() {
        return isValid;
    }

    public void setIsValid(Boolean valid) {
        isValid = valid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private boolean isIn(ApplicationUser friend){
        for(ApplicationUser user:this.friendsList){
            //if(user.getId().equals(friend.getId()))
            if(friend.equals(user))
                return true;
        }
        return false;
    }

    public ApplicationUser addFriend(ApplicationUser friend){

        if(isIn(friend))
            return friend;
        this.friendsList.add(friend);
        //friend.addFriend(this);
        return null;
    }



    public ApplicationUser removeFriend(ApplicationUser friend){
        if(!isIn(friend))
            return friend;
        if(friendsList.removeIf(user ->(user.getId().equals(friend.getId()))))
            return null;
        return friend;

    }

    @Override
    public boolean equals(Object o) {
        ApplicationUser ot = (ApplicationUser) o;
        // Compare the data members and return accordingly
        if (!(o instanceof ApplicationUser)) return false;
        return this.getId().equals(ot.getId()) && this.firstName.equals(ot.firstName)
                && this.lastName.equals(ot.lastName);
    }
}
