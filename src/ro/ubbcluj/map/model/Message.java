package ro.ubbcluj.map.model;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
//id-ul mesajului  from to
public class Message extends Entity<Long>{

    private ApplicationUser from;
    private List<ApplicationUser> to;
    private String message;
    private LocalDateTime date;
    private List<Message> reply;

    public Message(ApplicationUser from, List<ApplicationUser> to, String message, LocalDateTime date,Long id) {
        this.from = from;
        /*if(to.size() == 0)
            this.to = new ArrayList<ApplicationUser>();*/
         this.to = to;
        this.message = message;
        this.date = date;
        reply= new ArrayList<Message>();
        super.setId(id);

    }


    public ApplicationUser getFrom() {
        return from;
    }

    public void setFrom(ApplicationUser from) {
        this.from = from;
    }

    public List<ApplicationUser> getTo() {
        return to;
    }

    public void setTo(List<ApplicationUser> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public List<Message> getReply() {
        return reply;
    }

    public void setReply(List<Message> reply) {
        this.reply = reply;
    }

    public void addReply(Message reply){
        this.reply.add(reply);
    }
    public void addRecipient(ApplicationUser recipient){
        this.to.add(recipient);
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", reply=" + String.valueOf(reply.size()) + "   "+
                 "id=" + String.valueOf(this.getId())+
                '}';
    }
}
