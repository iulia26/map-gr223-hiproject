package ro.ubbcluj.map.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendshipRequestDTO<userID> {
  //se adauga si aici data
    UserDto<userID> from;
    UserDto<userID> to;
    FriendshipRequestStatus status;
    LocalDate date;
    Long id;

    public FriendshipRequestDTO(UserDto<userID> from, UserDto<userID> to, FriendshipRequestStatus status, LocalDate date) {
        this.from = from;
        this.to = to;
        this.status = status;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDto<userID> getFrom() {
        return from;
    }

    public void setFrom(UserDto<userID> from) {
        this.from = from;
    }

    public UserDto<userID> getTo() {
        return to;
    }

    public void setTo(UserDto<userID> to) {
        this.to = to;
    }

    public FriendshipRequestStatus getStatus() {
        return status;
    }

    public void setStatus(FriendshipRequestStatus status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
