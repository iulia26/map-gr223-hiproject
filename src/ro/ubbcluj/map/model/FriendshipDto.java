package ro.ubbcluj.map.model;

import java.time.LocalDate;

public class FriendshipDto<userID> {
    private UserDto<userID> user1,user2;
    private LocalDate date;

    public FriendshipDto(UserDto<userID> user1, UserDto<userID> user2, LocalDate date) {
        this.user1 = user1;
        this.user2 = user2;
        this.date = date;
    }

    public UserDto<userID> getUser1() {
        return user1;
    }

    public UserDto<userID> getUser2() {
        return user2;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "FriendshipDto{" +
                "user1=" + user1.toString() +
                ", user2=" + user2.toString() +
                ", date=" + date.toString() +
                '}';
    }
}
