package ro.ubbcluj.map.model;

import java.util.*;

public  class Graph {
   private ArrayList<ArrayList<Integer>> adjListArray;
   private int N;
    private  Integer max_Pathlength;
    public Graph(ArrayList<ArrayList<Integer>> adjListArray) {
        this.adjListArray = adjListArray;
        this.N=adjListArray.size();
        max_Pathlength=0;

    }

    /**
     *
     * @param v nodul sursa
     * @param visited nodurile visitate
     * @param components lisra cu nodurile din componenta conexa
     */
    private void DFSUtil(int v, boolean[] visited, ArrayList<Integer>components){
            visited[v]=true;
            components.add(v);
            for(int x:adjListArray.get(v)){
                if(!visited[x])
                    DFSUtil(x,visited,components);
            }
    }

    /**
     *
     * @return List of lists connected components
     */
    public  ArrayList<ArrayList<Integer>>  connectedComponents(){
        ArrayList<ArrayList<Integer>> componentsArray=new ArrayList<>();
        int V=adjListArray.size();
        boolean[] visited=new boolean[V];
        for(int v=0;v<V;v++){
                if(!visited[v]){
                    ArrayList<Integer> components=new ArrayList<Integer>();
                    DFSUtil(v,visited,components);
                    if(components.size()>1){
                            componentsArray.add(components);
                    }
                }
        }
        return componentsArray;
    }

    /**
     *
     * @param src nodul sursa
     * @param prev_len lungimea curenta a lantului
     * @param visited nodurile visitate
     */
    private void DFS(int src,int prev_len,boolean[] visited){
        visited[src]=true;
        int curr_len=0;
        for(int i=0;i<adjListArray.get(src).size();i++){
            int neighbour=adjListArray.get(src).get(i);
            if(!visited[neighbour]){
                curr_len=prev_len+1;
                DFS(neighbour,curr_len,visited);
            }
            if(max_Pathlength<curr_len)
                max_Pathlength=curr_len;
            curr_len=0;
        }
    }

    /**
     *
     * @return longest path in Graph
     */
    public Integer longestPath(){
       // System.out.println(adjListArray);
        for(int i=0;i<N;i++){
            boolean[] visited=new boolean[N];
            DFS(i,0,visited);
        }
       // System.out.println(max_Pathlength);
        return max_Pathlength;
    }
}
