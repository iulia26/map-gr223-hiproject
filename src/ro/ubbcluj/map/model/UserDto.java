package ro.ubbcluj.map.model;

public class UserDto<ID> {
    private ID userID;
    private String firstName;
    private String lastName;

    public UserDto(ID userID, String firstName, String lastName) {
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public ID getUserID() {
        return userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return userID.toString()+" "+firstName+" "+lastName;
    }
}
