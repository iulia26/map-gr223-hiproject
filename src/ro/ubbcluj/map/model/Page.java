package ro.ubbcluj.map.model;

public class Page {
    private UserDto userDto;
    private Integer messageCount;
    private Integer frCount;

    public Page(UserDto userDto, Integer messageCount, Integer frCount) {
        this.userDto = userDto;
        this.messageCount = messageCount;
        this.frCount = frCount;
    }

    public Page(UserDto userDto) {
        this.userDto = userDto;

    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public Integer getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

    public Integer getFrCount() {
        return frCount;
    }

    public void setFrCount(Integer sfrCount) {
        frCount = sfrCount;
    }
}
