package ro.ubbcluj.map.model;

import java.util.Objects;

public class Tuple <A,B>{
    private A first;
    private B second;

    public Tuple(A first, B second) {
        this.first = first;
        this.second = second;
    }

    public A getFirst() {
        return first;
    }

    public void setFirst(A first) {
        this.first = first;
    }

    public B getSecond() {
        return second;
    }

    public void setSecond(B second) {
        this.second = second;
    }

    @Override
    public int hashCode() {
        if(first.toString().compareTo(second.toString())<0)
            return Objects.hash(this.first,this.second);
        return Objects.hash(this.second,this.first);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Tuple)) return false;
        Tuple<A,B> tuple=(Tuple<A,B>)obj;

        return (this.getFirst().equals(tuple.getFirst()) && this.getSecond().equals(tuple.getSecond()))
                || (this.getFirst().equals(tuple.getSecond()) && this.getSecond().equals(tuple.getFirst()));
    }

    public boolean isEmpty() {
        return first==null || second==null;
    }
}