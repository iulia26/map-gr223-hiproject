package ro.ubbcluj.map.model;

public enum FriendshipRequestStatus {
    APPROVED("approved"),
    REJECTED("rejected"),
    PENDING("pending");

    private String status;

    FriendshipRequestStatus(String status) {
        this.status = status;
    }

    public String getStatusValue() {
        return status;
    }

    public static FriendshipRequestStatus getEnumValue(String status){
            switch (status){
                case "approved":
                    return FriendshipRequestStatus.APPROVED;
                case "pending":
                    return FriendshipRequestStatus.PENDING;
                case "rejected":
                    return FriendshipRequestStatus.REJECTED;
                default:
                    break;
            }
            return null;
    }
}
