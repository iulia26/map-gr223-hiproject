package ro.ubbcluj.map.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class MessageDTO {
    UserDto<String> from;
    List<UserDto<String>> to;
    String message;
    LocalDateTime dateTime;
    MessageDTO repliedTo;
    Long id;
    public MessageDTO(UserDto<String> from, List<UserDto<String>> to, String message, LocalDateTime dateTime,Long id) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.dateTime = dateTime;
        repliedTo=null;
        this.id=id;
    }

    public UserDto<String> getFrom() {
        return from;
    }

    public void setFrom(UserDto<String> from) {
        this.from = from;
    }

    public List<UserDto<String>> getTo() {
        return to;
    }

    public void setTo(List<UserDto<String>> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public MessageDTO getRepliedTo() {
        return repliedTo;
    }

    public void setRepliedTo(MessageDTO repliedTo) {
        this.repliedTo = repliedTo;
    }

    @Override
    public String toString() {
        String repliedTo="";
        if(this.repliedTo!=null){
            repliedTo+="[REPLIED to:"+this.repliedTo.getMessage()+"  "+this.repliedTo.getDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))+"] ";
        }
        return repliedTo+String.valueOf(id)+" "+this.dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))+"    "+this.from+"->"+this.to+":"+this.message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
