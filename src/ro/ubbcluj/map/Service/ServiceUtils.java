package ro.ubbcluj.map.Service;

import java.util.List;

public class ServiceUtils {

    /**
     *
     * @param collection List<T>
     * @param value type T
     * @return index of value in collection
     */
    public static int getIndexOFvalueInList(List<? extends Object> collection, Object value) {
        int result = 0;
        for (Object entry : collection) {
            if (entry.equals(value)) return result;
            result++;
        }
        return -1;

    }
}
