package ro.ubbcluj.map.Service;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.Tuple;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.paging.Page;
import ro.ubbcluj.map.repository.paging.PageRequest;
import ro.ubbcluj.map.repository.paging.Pageable;
import ro.ubbcluj.map.repository.paging.PagedFriendshipRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class FriendshipService {

    protected PagedFriendshipRepository<Tuple<String,String>, Friendship> repo;

    public FriendshipService(PagedFriendshipRepository<Tuple<String,String>, Friendship> repo) {
        this.repo = repo;
    }

    public Friendship add(Friendship friendship)  {
        if(friendship==null)
            throw new IllegalArgumentException("user and friend must not be null");
        if(repo.save(friendship)==null)
            return null;
        return friendship;
    }

    public Friendship delete(Friendship friendship){
        if(friendship==null)
            throw new IllegalArgumentException("user and friend must not be null");
        if(repo.delete(friendship)==null)
            return null;
        return friendship;
    }

    /**
     *
     * @return only active friendships
     */
    public List<Friendship> getAll(){
        List<Friendship> all = new ArrayList<>();
        Pageable current = new PageRequest(0,10);
        Page<Friendship> pageResource= repo.findAll(current);
        while(pageResource != null){
            all.addAll(pageResource.getContent().collect(Collectors.toList()));
            current = pageResource.nextPageable();
            pageResource = repo.findAll(current);
        }
        return all;
    }
}
