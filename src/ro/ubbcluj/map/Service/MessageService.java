package ro.ubbcluj.map.Service;

import ro.ubbcluj.map.model.Message;
import ro.ubbcluj.map.model.SortingOrder;
import ro.ubbcluj.map.repository.dbo.MessageRepoDboPaginated;
import ro.ubbcluj.map.repository.paging.Page;
import ro.ubbcluj.map.repository.paging.PageContent;
import ro.ubbcluj.map.repository.paging.PageRequest;
import ro.ubbcluj.map.repository.paging.Pageable;

import java.util.stream.Collectors;

public class MessageService {

    protected MessageRepoDboPaginated repo;

    public MessageService(MessageRepoDboPaginated repo) {
        this.repo = repo;
    }


    private int pageSizeConversationMessages = 12;


    public int getPageSizeConversationMessages() {
        return pageSizeConversationMessages;
    }


    Page<Message> findTwoUsersConversation(String idUser1, String idUser2, Pageable pageRequest, SortingOrder order){
        Page<Message> conversation = repo.findTwoUsersConversation(idUser1,idUser2,order,pageRequest);
        if(conversation == null)return null;
        return conversation;
    }

    Page<String> getUserConversationPartnersID(String userID,Pageable pageRequest){
        Page<String> partners = repo.getUserConversationPartnersID(userID,pageRequest);
        if(partners == null) {return null;}
        return  partners;
    }

}
