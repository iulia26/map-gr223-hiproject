package ro.ubbcluj.map.Service;

import ro.ubbcluj.map.model.*;
import ro.ubbcluj.map.model.validators.Validator;
import ro.ubbcluj.map.myException.IDisAlreadyTakenException;
import ro.ubbcluj.map.myException.InsufficientDataToExecuteTaskException;
import ro.ubbcluj.map.myException.RepoError;
import ro.ubbcluj.map.repository.FriendshipRequestRepository;
import ro.ubbcluj.map.repository.MessageRepository;
import ro.ubbcluj.map.repository.dbo.ConversationDbo;
import ro.ubbcluj.map.utils.AESalgorithm.EncryptorAesGcmPassword;
import ro.ubbcluj.map.utils.events.Event;
import ro.ubbcluj.map.utils.events.EventType;
import ro.ubbcluj.map.utils.events.NetworkServiceTask;
import ro.ubbcluj.map.utils.observer.Observable;
import ro.ubbcluj.map.utils.observer.Observer;
import ro.ubbcluj.model.validators.FriendshipTupleIdValidator;
import ro.ubbcluj.model.validators.MessagesValidator;
import ro.ubbcluj.model.validators.UserStringIdValidator;



import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class NetworkService implements Observable {

    private FriendshipService srvFriendship;
    private final Validator<Friendship> validatorFriendship;
    private UserService srvUser;
    private final Validator<ApplicationUser> validatorUser;
    private MessageRepository<Long, Message> repoMessage;
    private Validator<Message> messageValidator;
    private FriendshipRequestRepository<Long,FriendshipRequest> repoRequests;
    private ConversationDbo repoConversation;
    private List<Observer> observers;

    public NetworkService(FriendshipService serviceFriendship, FriendshipTupleIdValidator friendshipTupleIdValidator, UserService serviceUser, UserStringIdValidator userStringIdValidator, MessageRepository<Long, Message> messageRepository, MessagesValidator messagesValidator,
                          FriendshipRequestRepository<Long, FriendshipRequest> repoFriendshipRequest,ConversationDbo conversationDbo) {
        this.srvFriendship = serviceFriendship;
        this.validatorFriendship = friendshipTupleIdValidator;
        this.srvUser = serviceUser;
        this.validatorUser = userStringIdValidator;
        this.repoMessage = messageRepository;
        this.messageValidator = messagesValidator;
        this.repoRequests = repoFriendshipRequest;
        this.repoConversation = conversationDbo;
        this.observers = new ArrayList<>();
        //initData();
    }

    /**
     * used only for file and inMemory repo's
     */
    private void initData(){
        List<Friendship> friendships=this.srvFriendship.getAll();
        for(Friendship elem:friendships)
        {
            srvUser.addFriend(elem.getUser1ID(),elem.getUser2ID());
            srvUser.addFriend(elem.getUser2ID(),elem.getUser1ID());
        }
    }

    /**
     *
     * @param IdUser1 user string id
     * @param IdUser2 user string id
     * @return true successfully added friendship,false otherwise
     * @throws InsufficientDataToExecuteTaskException
     */
    public boolean addFriendship(String IdUser1, String IdUser2) throws InsufficientDataToExecuteTaskException {

        Friendship friendship=new Friendship(IdUser1,IdUser2, LocalDate.now());
        validatorFriendship.validate(friendship);
        ApplicationUser user1=srvUser.findOne(IdUser1);
        ApplicationUser user2=srvUser.findOne(IdUser2);
        if(user1==null || user2==null)//verific daca utilizatorii exista
            throw new InsufficientDataToExecuteTaskException("Invalid users\n");

        if(srvFriendship.add(friendship)==null) {//prietenia s a adaugat
            if (srvUser.addFriend(IdUser1, IdUser2) && srvUser.addFriend(IdUser2, IdUser1))
                return true;
        }
        return false;
    }

    /**
     * Converts an ApplicationUser object to a UserDto object
     * @param applicationUser ApplicationUser
     * @return
     */
    private UserDto<String> fromApplicationUser(ApplicationUser applicationUser){
        return  new UserDto<String>(applicationUser.getId(),applicationUser.getFirstName(),applicationUser.getLastName());
    }

    /**
     *  This function creates and return a friendshipDto from a friendship
     * @param friendship
     * @return
     */
    private FriendshipDto<String> createFriendshipDto(Friendship friendship) {
        UserDto<String> user1 = fromApplicationUser(srvUser.findOne(friendship.getUser1ID()));
        UserDto<String> user2 = fromApplicationUser(srvUser.findOne(friendship.getUser2ID()));
        return new FriendshipDto<String>(user1,user2,friendship.getStartDate());

    }

    /**
     * Functia primeste un iD si returneaza lista pritenilor lui
     * @param userID
     * @return List<FriendshipDto>
     * @throws InsufficientDataToExecuteTaskException
     * @throws RepoError
     */
    public List<FriendshipDto<String>> getFriendshipList(String userID) throws InsufficientDataToExecuteTaskException, RepoError {
        if (userID == null)
            throw new InsufficientDataToExecuteTaskException("Id can't be null\n");
        if (this.srvUser.findOne(userID) == null)
            throw new RepoError("Inexistent user\n");
        List<Friendship> friendshipList = this.srvFriendship.getAll();
        return friendshipList.stream()
                .filter(friendship -> friendship.getUser1ID().equals(userID) || friendship.getUser2ID().equals(userID))
                .map(this::createFriendshipDto)
                .collect(Collectors.toList());
    }

    /**
     * Functia primeste un id si o data
     * Returneaza o lista cu prietenii dupa id si data
     * @param idUser
     * @param month
     * @return
     * @throws InsufficientDataToExecuteTaskException
     * @throws RepoError
     */
    public List<FriendshipDto<String>> getFriendshipListByDate(String idUser, int month) throws InsufficientDataToExecuteTaskException, RepoError {
        if (idUser == null)
            throw new InsufficientDataToExecuteTaskException("Id can't be null\n");
        if (this.srvUser.findOne(idUser) == null)
            throw new RepoError("Inexistent user\n");
        if (month <1 || month >12)
            throw new InsufficientDataToExecuteTaskException("This month does not exist\n");

        List<Friendship> friendshipList = this.srvFriendship.getAll();
        return friendshipList.stream()
                .filter(friendship -> (friendship.getUser1ID().equals(idUser) || friendship.getUser2ID().equals(idUser)) && friendship.getStartDate().getMonthValue()==month)
                .map(this::createFriendshipDto)
                .collect(Collectors.toList());
    }

    public Friendship deleteFriendship(String idUser1, String idUser2){
        Friendship friendship=new Friendship(idUser1,idUser2);
        validatorFriendship.validate(friendship);
        if(srvFriendship.delete(friendship)==null) {
            if(srvUser.deleteFriend(idUser1,idUser2) && srvUser.deleteFriend(idUser2,idUser1)){
                notifyObservers(new NetworkServiceTask(EventType.FRIENDSHIP));
                return null;}
            return friendship;
        }
        return friendship;
    }

    /**
     *
     * @param userDto
     * @param accountPassword
     * @return null -successfully added user,ApplicationUser object otherwise
     * @throws ro.ubbcluj.map.model.validators.ValidationException,IllegalArgumentException,IDisAlreadyTakenException
     */
    public ApplicationUser addUser(UserDto<String> userDto,String accountPassword) throws IDisAlreadyTakenException {
        ApplicationUser user=new ApplicationUser(userDto.getFirstName(),userDto.getLastName());
        user.setId(userDto.getUserID());
        this.validatorUser.validate(user);
        if(srvUser.add(user) != null)
            throw new IDisAlreadyTakenException("Couldn't add user");
        String encryptedTextBase64 = "";
        try {
            encryptedTextBase64 = EncryptorAesGcmPassword.encrypt(accountPassword.getBytes(StandardCharsets.UTF_8), srvUser.repo.getPasswordEncryptionPass());
        } catch (Exception e) {
            e.printStackTrace();
        }
        srvUser.repo.setUserPassword(encryptedTextBase64,userDto.getUserID());
        return null;
    }

    /**
     *
     * @param userID
     * @return  null-successfully removed user,user otherwise
     * @throws ro.ubbcluj.map.model.validators.ValidationException,IllegalArgumentException
     */
    public ApplicationUser deleteUser(String userID){

        ApplicationUser toValidate=new ApplicationUser("none","none");
        toValidate.setId(userID);
        this.validatorUser.validate(toValidate);
        List<Friendship> all=srvFriendship.getAll();
        for(Friendship elem:all){
            if (elem.getUser1ID().equals(userID)) {
                srvUser.deleteFriend(elem.getUser2ID(), userID);
                srvFriendship.delete(elem);
            }
            else if(elem.getUser2ID().equals(userID)){
                srvUser.deleteFriend(elem.getUser1ID(),userID);
                srvFriendship.delete(elem);
            }
        }
        ApplicationUser originalUser= srvUser.findOne(userID);
        return srvUser.delete(originalUser);
    }

    /**
     *
     * @return list of connected components in network's graph
     */
    private ArrayList<ArrayList<Integer>> communities(){
        List<String>IDs=new ArrayList<>();
        return new Graph(this.convertToGraph(IDs)).connectedComponents();
    }

    /**
     *
     * @return number of connected components in network's graph
     */
    public Integer communitiesCounter(){
        return communities().size();
    }

    /**
     *
     * @param listID stores users IDs
     * @return adjacency list of the graph formed by the network
     */
    private ArrayList<ArrayList<Integer>> convertToGraph(List<String> listID) {


        List<ApplicationUser> users=this.srvUser.getAll();
        ArrayList<ArrayList<Integer>> adjListArray = new ArrayList<>();
        int vertices = users.size();
        for (int i = 0; i < vertices; i++) {
            adjListArray.add(i, new ArrayList<>());
        }

        //adaug valorile in set
        List<String> listAUX=new ArrayList<>();
        for (ApplicationUser user: users) {
            listAUX.add(user.getId());
        }

        for (ApplicationUser user : users) {
            int userIdIndex = ServiceUtils.getIndexOFvalueInList(listAUX, user.getId());

            List<Friendship> all=srvFriendship.getAll();

            for (Friendship friendship : all) {
                int friendIndex=-1;
                if (friendship.getUser1ID().equals(user.getId())) {
                    friendIndex= ServiceUtils.getIndexOFvalueInList(listAUX,friendship.getUser2ID());
                }
                else {
                    if (friendship.getUser2ID().equals(user.getId()))
                        friendIndex= ServiceUtils.getIndexOFvalueInList(listAUX,friendship.getUser1ID());
                }
                if(friendIndex>-1)
                    adjListArray.get(userIdIndex).add(friendIndex);

            }
        }
        listID.addAll(listAUX);
        return adjListArray;
    }

    /**
     *
     * @return list with members of the connected component with the longest path in the  network's graph
     * @throws InsufficientDataToExecuteTaskException if there are no friendship relationships formed yet
     */
    public List<UserDto<String>> friendliestCommunity() throws InsufficientDataToExecuteTaskException {
        List<String> IDs=new ArrayList<>();
        var graph=convertToGraph(IDs);
        int maxPath= -1;
        int mostSociableComunityIndex=-1;
        int cnt=0;
        var comunities=this.communities();
        if(comunities.size()>0){
            for (var comunity:comunities){
                ArrayList<ArrayList<Integer>> comunityAdjList=new ArrayList<ArrayList<Integer>>();
                //indexarea utilizatorilor va fi alta pentru noua lista de adiacenta a componentei conexe
                List<Integer> codes=new ArrayList<>();
                for(var node:comunity) {
                    codes.add(node);
                }
                for(var node:comunity) {
                    ArrayList<Integer> adjacentNodes=new ArrayList<>();
                    for(Integer neighbour:graph.get(node)){
                        adjacentNodes.add(ServiceUtils.getIndexOFvalueInList(codes,neighbour));
                    }
                    comunityAdjList.add(adjacentNodes);

                }
                //calculez drumul de lungime maxima

                Graph G=new Graph(comunityAdjList);
                var longestPath=G.longestPath();
                if(longestPath>maxPath){
                    maxPath=longestPath;
                    mostSociableComunityIndex=cnt;
                }
                cnt++;
            }
            List<UserDto<String>> result=new ArrayList<UserDto<String>>();
            var component=comunities.get(mostSociableComunityIndex);
            for(var index:component){
                ApplicationUser user=srvUser.findOne(IDs.get(index));
                result.add(new UserDto<String>(user.getId(),user.getLastName(),user.getFirstName()));
            }
            return result;
        }
        else
            throw new InsufficientDataToExecuteTaskException("Inca nu s-au format comunitati in reteaua de socializare!\n");
    }

    /**
     *
     * @return list of UserDto's
     */
    public List<UserDto<String>> getAllUsers(){
        List<UserDto<String>>users=new ArrayList<>();
        for(ApplicationUser user:srvUser.getAll())
            users.add(new UserDto<String>(user.getId(),user.getFirstName(),user.getLastName()));
        return users;
    }

    /**
     * Converts a Message object to MessageDTO object
     * @param message Message
     * @return MessageDTO
     */
    public MessageDTO fromMessage(Message message){
        UserDto<String> from = new UserDto<String>(message.getFrom().getId(),message.getFrom().getFirstName(),message.getFrom().getLastName());

        List<UserDto<String>> to = message.getTo().stream()
                                .map(applicationUser ->new UserDto<String>(applicationUser.getId(),applicationUser.getFirstName(),applicationUser.getLastName()))
                                .collect(Collectors.toList());
        return  new MessageDTO(from,to,message.getMessage(),message.getDate(),message.getId());
    }



   /**
     * Gives conversation history between two users in chronological order
     * @param idUser1 String
     * @param idUser2 String
     * @return List<MessageDTO>
     */
    public List<MessageDTO> getConversationHistory(String idUser1,String idUser2){
        if(srvUser.findOne(idUser1) == null || srvUser.findOne(idUser2) == null)
            throw new IllegalArgumentException("Nonexistent user(s)");


       Iterable<Message> messageIterable = repoMessage.findTwoUsersConversation(idUser1,idUser2);
        return StreamSupport.stream(messageIterable.spliterator(),false)
                                                                    .map(this::fromMessage)
                                                                     .sorted(Comparator.comparing(MessageDTO::getDateTime))
                                                                     .collect(Collectors.toList());

    }

    public List<MessageDTO> getConversationHistoryByDate(String idUser1, String idUser2, LocalDateTime from, LocalDateTime to) {
        if (this.srvUser.findOne(idUser1) != null && this.srvUser.findOne(idUser2) != null) {
            Iterable<Message> messageIterable = this.repoMessage.findTwoUsersConversation(idUser1, idUser2);
            return new ArrayList(StreamSupport.stream(messageIterable.spliterator(), false)
                                    .map(this::fromMessage)
                                    .filter((x) -> {
                                            return x.getDateTime().isAfter(from) && x.getDateTime().isBefore(to) && ((String)x.getFrom().getUserID()).equals(idUser2);
                                     }).sorted(Comparator.comparing(MessageDTO::getDateTime))
                                        .toList());
        } else {
            throw new IllegalArgumentException("Nonexistent user(s)");
        }
    }

    public Integer getMessagesCountByDate(String idUser,LocalDateTime from,LocalDateTime to){
        Iterable<Message> messageIterable = repoMessage.findUserMessages(idUser);
        Integer count = new ArrayList<>(StreamSupport.stream(messageIterable.spliterator(), false)
                .map(this::fromMessage)
                .filter(x-> x.getDateTime().isAfter(from) && x.getDateTime().isBefore(to)).toList()).size();
        return count;
    }

    public Integer getUnreadMessagesCount(String iduser){
        Integer total=0;
        List<UserDto<String>> conversationPartners = getAllUserConversationPartners(iduser);
        for (UserDto<String> partner : conversationPartners){
            List<MessageDTO> conversation = getConversationHistory(iduser, partner.getUserID());
            int conversationUnread = conversation.size();
            total += findOneConversationUnreadMessages(new ConversationDTO(new UserDto<String>(partner.getUserID(),null,null),new UserDto<String>(iduser, null,null),0L));
        }
        return total;
    }

    public List<FriendshipDto> getFriendshipsByDate(String idUser, LocalDateTime from,LocalDateTime to) throws InsufficientDataToExecuteTaskException, RepoError {
        if (idUser == null)
            throw new InsufficientDataToExecuteTaskException("Id can't be null\n");
        if (this.srvUser.findOne(idUser) == null)
            throw new RepoError("Inexistent user\n");

        List<Friendship> friendshipList = this.srvFriendship.getAll();
        return friendshipList.stream()
                .filter(friendship -> (friendship.getUser1ID().equals(idUser) || friendship.getUser2ID().equals(idUser)) && friendship.getStartDate().isAfter(from.toLocalDate()) && friendship.getStartDate().isBefore(to.toLocalDate()))
                .map(this::createFriendshipDto)
                .collect(Collectors.toList());
    }
    /**
     * Functia primeste un ID
     * Returneaza user ul care are acel ID
     * @param userID
     * @return
     */
    public  UserDto<String> findUserById(String userID) {
        ApplicationUser applicationUser = this.srvUser.findOne(userID);
        return new UserDto<String>(applicationUser.getId(),applicationUser.getFirstName(),applicationUser.getLastName());
    }

    public FriendshipRequestDTO<String> existsPendingFriendshipRequest(Tuple<String,String> usersPair){
        Long friendshipRequestId = repoRequests.findOnePendingRequestsByUserIdTuple(usersPair) ;
        if(friendshipRequestId != null) {
            FriendshipRequest friendshipRequest = repoRequests.findOne(friendshipRequestId);
            if (friendshipRequest.getStatus().equals(FriendshipRequestStatus.PENDING)) {
                return new FriendshipRequestDTO<String>(new UserDto<String>(friendshipRequest.getFrom().getId()
                        , friendshipRequest.getFrom().getFirstName()
                        , friendshipRequest.getFrom().getLastName())
                        , new UserDto<String>(friendshipRequest.getTo().getId()
                        , friendshipRequest.getTo().getFirstName()
                        , friendshipRequest.getTo().getLastName())
                        , friendshipRequest.getStatus()
                        , friendshipRequest.getDate());
            }

        }
        return null ;
    }

    /**
     *Finds all the messages that the user did not replied to yet
     * @param userId String user's id
     * @return List<MessageDTO> not replied user messages
     */
    public List<MessageDTO> getUserReceivedMessages(String userId){

        Predicate<Message> isInMessageRecipientsList = message -> message.getTo().stream().anyMatch(user-> user.getId().equals(userId));
        Predicate<Message> didNotReplyToThisMessageYet = message -> message.getReply().stream().noneMatch(msg->msg.getFrom().getId().equals(userId) );

        return StreamSupport.stream(repoMessage.findUserMessages(userId).spliterator(),false)
                 .filter(isInMessageRecipientsList.and(didNotReplyToThisMessageYet) )
                 .map(this::fromMessage)
                 .collect(Collectors.toList());

    }

    public MessageDTO repliesTo(Long messageId){
        Long Id  = repoMessage.repliesTo(messageId);
        if(Id == null)
            return null;
        return fromMessage(repoMessage.findOne(Id));
    }

    /**
     *
     * @param idUser String a user id
     * @throws IllegalArgumentException if the user doesn't exist in the database
     * @return true if the log in was successfull  and false otherwise
     */
    public boolean logIN(String idUser,String password) {
        if(idUser.length() == 0 || password.length() == 0) return false;
        String userPass = getUserPassword(idUser);
        if(userPass == null) return false;
        return srvUser.findOne(idUser) != null && userPass.equals(password);
    }

    public MessageDTO findOneMessageById(Long messageId){
        Message original = repoMessage.findOne(messageId);
        return fromMessage(original);
    }
    /**
     *Sends  message to users
     * @param messageDTO MessageDTO
     * @throws RepoError
     * @return true if the message was saved and false otherwise
     */

    public boolean sendMessage(MessageDTO messageDTO)  {
        if(this.srvUser.findOne(messageDTO.getFrom().getUserID())==null)
            throw new IllegalArgumentException("Nonexistent user!");
        messageDTO.getTo().forEach(to->{if(this.srvUser.findOne(to.getUserID())==null)
            throw new IllegalArgumentException("Couldn't send message because one user in the recipient list does not exist");
        });
        List<ApplicationUser> recipients = messageDTO.getTo().stream()
                                            .map(userDto->new ApplicationUser(userDto.getFirstName(),userDto.getLastName(),userDto.getUserID()))
                                            .collect(Collectors.toList());
        Message messageToSave = new Message(
                new ApplicationUser(messageDTO.getFrom().getFirstName(),messageDTO.getFrom().getLastName(),messageDTO.getFrom().getUserID())
                ,recipients
                ,messageDTO.getMessage()
                ,LocalDateTime.now()
                ,0L);

        boolean sem = repoMessage.saveMessage(messageToSave) == null;
        notifyObservers(new NetworkServiceTask(EventType.MESSAGE));
        return sem;

    }



    /**
     *Replies to a message
     * @param messageDTO MessageDTO
     * @return true for successfully sent reply,false otherwise
     */
    public boolean replyMessage(MessageDTO messageDTO){


            Message msgReply = new Message(new ApplicationUser(messageDTO.getFrom().getFirstName(),messageDTO.getFrom().getLastName(),messageDTO.getFrom().getUserID())
                    , List.of(new ApplicationUser(messageDTO.getTo().get(0).getFirstName(), messageDTO.getTo().get(0).getLastName(), messageDTO.getTo().get(0).getUserID()))
                    ,messageDTO.getMessage()
                    ,LocalDateTime.now()
                    ,0L);
        boolean sem = repoMessage.saveReplyMessage(messageDTO.getRepliedTo().getId(), msgReply) == null;
        notifyObservers(new NetworkServiceTask(EventType.MESSAGE));
        return sem && msgReply.getId() > 0;
    }

    /**
     *Reply to all - sends a user's reply to everyone on the original message thread
     *
     * @param originalMessage MessageDTO - message that we want to reply to
     * @param senderId - the sender id
     * @param replyText - the reply text  for the @param originalMessage
     * @return true on success,false otherwise
     */
    public boolean replyAll(MessageDTO originalMessage,String senderId,String replyText){

            ApplicationUser from = srvUser.findOne(senderId);
            //sa scot userul din lista de destinatari si sa aadaug in lista de destinatari
            List<ApplicationUser> to =originalMessage.getTo().stream()
                                                        .filter(stringUserDto -> !stringUserDto.getUserID().equals(senderId))
                                                        .map(userDto -> srvUser.findOne(userDto.getUserID()))
                                                        .collect(Collectors.toList());
            to.add(srvUser.findOne(originalMessage.getFrom().getUserID()));
            Message reply = new Message(from,to,replyText,LocalDateTime.now(),0L);
            boolean sem = repoMessage.saveReplyMessage(originalMessage.getId(), reply) == null;
        notifyObservers(new NetworkServiceTask(EventType.MESSAGE));
        return sem && reply.getId() > 0;
    }


    public List<UserDto<String>> getAllUserConversationPartners(String userID){
        return StreamSupport.stream(repoMessage.getUserConversationPartnersID(userID).spliterator(),false)
                .map(userIDstring -> {
                    return  fromApplicationUser(srvUser.findOne(userIDstring));
                })
                .collect(Collectors.toList());
    }



   /**
     * Primeste un user Id si returneaza toate cererile de prietenie adresate acelui user
     * @param userID user's id
     * @return a list with all user's friend requests
     */
    public List<FriendshipRequestDTO<String>> getAllPendingFriendshipRequestForOneUser(String userID) {

        List<FriendshipRequestDTO<String>> result = new ArrayList<>();
        repoRequests.pendingFriendshipRequests(userID).forEach(request->result.add(new FriendshipRequestDTO<>(new UserDto<>(request.getFrom().getId(),request.getFrom().getFirstName(),request.getFrom().getLastName()), new UserDto<>(request.getTo().getId(),request.getTo().getFirstName(),request.getTo().getLastName()),request.getStatus(),request.getDate())));

        return result;
    }

    /**
     *
     * @param userID user's id
     * @return all friendshipReuqsts history
     */
    public List<FriendshipRequestDTO<String>> getAllFriendshipRequestsForUser(String userID) {
        List<FriendshipRequestDTO<String>> resultList = new ArrayList<>();
        repoRequests.findAllDespiteStatus(userID).forEach(request->resultList.add(new FriendshipRequestDTO<>(new UserDto<>(request.getFrom().getId(),request.getFrom().getFirstName(),request.getFrom().getLastName()), new UserDto<>(request.getTo().getId(),request.getTo().getFirstName(),request.getTo().getLastName()),request.getStatus(),request.getDate())));
        return resultList;
    }

    /**
     * Primeste doua id uri si trimite o cerere de prietenie de la primul la al doilea user
     * @param requestDTO FriendshipRequestDTO<String>  where String is the user id value type
     * @throws InsufficientDataToExecuteTaskException
     * @throws RepoError
     * Preconditions: users in requestDTO must be valid
     */
    public void sendFriendshipRequest(FriendshipRequestDTO<String> requestDTO) throws InsufficientDataToExecuteTaskException, RepoError{

        if(srvUser.findOne(requestDTO.getFrom().getUserID()) == null) throw new InsufficientDataToExecuteTaskException("Inexistent user!");
        if(srvUser.findOne(requestDTO.getFrom().getUserID()) == null) throw new InsufficientDataToExecuteTaskException("Inexistent user!");
        Tuple<String,String> id = new Tuple<>(requestDTO.getFrom().getUserID(),requestDTO.getTo().getUserID());
        if(srvFriendship.repo.findOne(id) !=null )throw new InsufficientDataToExecuteTaskException("These users are already friends!");
        ApplicationUser user1 = new ApplicationUser(requestDTO.getFrom().getFirstName(),requestDTO.getFrom().getLastName(),requestDTO.getFrom().getUserID());
        ApplicationUser user2 = new ApplicationUser(requestDTO.getTo().getFirstName(),requestDTO.getTo().getLastName(),requestDTO.getTo().getUserID());
        FriendshipRequest newRequest = new FriendshipRequest(user1,user2,0L,LocalDate.now());

        if(repoRequests.save(newRequest) != null)
            throw new InsufficientDataToExecuteTaskException("A friendship request between these two users was already sent!");
        notifyObservers(new NetworkServiceTask(EventType.FriendshipRequests));
    }

    /**
     * Functia schimba starea unei cereri de prietenie iar in cazul acceptarii acesteia, o adauga in lista de prietenii
     * requestDTO FriendshipRequestDTO<String> String is the user id value type
     * @throws InsufficientDataToExecuteTaskException
     * @throws RepoError
     * Preconditions:users in requestDTO must be valid
     */
    public void updateFriendshipRequestStatus(FriendshipRequestDTO<String> requestDTO) throws InsufficientDataToExecuteTaskException, RepoError{

        if(srvUser.findOne(requestDTO.getFrom().getUserID()) == null) throw new InsufficientDataToExecuteTaskException("Inexistent user!");
        if(srvUser.findOne(requestDTO.getTo().getUserID()) == null) throw new InsufficientDataToExecuteTaskException("Inexistent user!");
        Tuple<String,String> id = new Tuple<>(requestDTO.getFrom().getUserID(),requestDTO.getTo().getUserID());
        Long requestId = repoRequests.findOnePendingRequestsByUserIdTuple(id);
        if ( requestId == null )
            throw new InsufficientDataToExecuteTaskException("This friendshipRequest does not exist\n");
        FriendshipRequest friendshipRequest = repoRequests.findOne(requestId);
        if (requestDTO.getStatus().equals(FriendshipRequestStatus.REJECTED)){
            friendshipRequest.setStatus(FriendshipRequestStatus.REJECTED);
            repoRequests.update(friendshipRequest);
        }
        else if (requestDTO.getStatus().equals(FriendshipRequestStatus.APPROVED)){
            friendshipRequest.setStatus(FriendshipRequestStatus.APPROVED);
            if (addFriendship(requestDTO.getFrom().getUserID(),requestDTO.getTo().getUserID())) repoRequests.update(friendshipRequest);
        }
        notifyObservers(new NetworkServiceTask(EventType.FriendshipRequests));
    }

    /**
     * Converts a FriendshipRequestDTO<String> to an entity of tyoe FriendshipRequest
     * @param requestDTO FriendshipRequestDTO<String>
     * @return FriendshipRequest object
     */
    private FriendshipRequest fromFriendshipRequestDTO(FriendshipRequestDTO<String> requestDTO){
        return new FriendshipRequest(new ApplicationUser(requestDTO.getFrom().getFirstName(),requestDTO.getFrom().getLastName(),requestDTO.getFrom().getUserID())
                ,new ApplicationUser(requestDTO.getTo().getFirstName(),requestDTO.getTo().getLastName(),requestDTO.getTo().getUserID())
                ,requestDTO.getId()
                ,requestDTO.getDate());
    }

    /**
     * Coordinates the delete operation of a friendshipRequest
     * @param requestDTO A friendshipRequest dataTransfer object of type FriendshipRequestDTO<String>
     * @return true for a successful delete,false otherwise
     * @throws InsufficientDataToExecuteTaskException if the friendshipRequest id does not exist in the database
     */
    public boolean deletePendingFriendshipRequest(FriendshipRequestDTO<String> requestDTO)  throws InsufficientDataToExecuteTaskException{
        if(srvUser.findOne(requestDTO.getFrom().getUserID()) == null) throw new InsufficientDataToExecuteTaskException("Inexistent user!");
        if(srvUser.findOne(requestDTO.getTo().getUserID()) == null) throw new InsufficientDataToExecuteTaskException("Inexistent user!");
        Long requestToDeleteId = repoRequests.findOnePendingRequestsByUserIdTuple(new Tuple<String,String>(requestDTO.getFrom().getUserID(),requestDTO.getTo().getUserID()));
        if(requestToDeleteId == null)
            throw new InsufficientDataToExecuteTaskException("This friendshipRequest does not exist\n");
        FriendshipRequest requestToDelete = repoRequests.findOne(requestToDeleteId);
        return repoRequests.delete(requestToDelete) == null;
    }

    private Conversation fromConversationDTO(ConversationDTO conversationDTO){
        return new Conversation(new ApplicationUser(null,null,conversationDTO.getUser1().getUserID())
                                ,new ApplicationUser(null,null,conversationDTO.getUser2().getUserID())
                                ,conversationDTO.getLastReadMessageID()
                                ,conversationDTO.getConversationID());
    }

    private ConversationDTO fromConversation(Conversation conversation){
        return  new ConversationDTO(new UserDto<String>(conversation.getUser1().getId(),null,null)
                                    ,new UserDto<String>(conversation.getUser2().getId(),null,null)
                                    ,conversation.getLastReadMessageID()
                                    ,conversation.getConversationID());
    }
    public void setConversationLastReadMessage(ConversationDTO conversationDTO){
        Conversation  conversation = fromConversationDTO(conversationDTO);
        List<Message> messageList = StreamSupport.stream( repoMessage.findTwoUsersConversation(conversation.getUser1().getId(),conversation.getUser2().getId()).spliterator(),false)
                                                        .sorted(Comparator.comparing(Message::getDate))
                                                        .collect(Collectors.toList());
        Long lastRead = messageList.get(messageList.size() - 1).getId();
        conversation.setLastReadMessageID(lastRead);
        Long conversationID =  repoConversation.findOnesID(new Tuple<String,String>
                (conversationDTO.getUser1().getUserID()
                        ,conversationDTO.getUser2().getUserID()));
        if(conversationID != null){
            conversation.setConversationID(conversationID);
            repoConversation.update(conversation);
        }
        else {
            repoConversation.save(conversation);
        }
    }

    /**
     *
     * @param conversationDTO
     * The second user from conversationDTO should correspond to the logged-in user
     * @return
     */
    public int findOneConversationUnreadMessages(ConversationDTO conversationDTO){
        Long conversationID =  repoConversation.findOnesID(new Tuple<String,String>
                (conversationDTO.getUser1().getUserID()
                        ,conversationDTO.getUser2().getUserID()));
        if(conversationID == null){setConversationLastReadMessage(conversationDTO);
                                    conversationID = repoConversation.findOnesID(new Tuple<String,String>
                                            (conversationDTO.getUser1().getUserID()
                                                    ,conversationDTO.getUser2().getUserID()));
        }
        Long lastRead = repoConversation.findOne(conversationID).getLastReadMessageID();
        boolean start = false;
        int count = 0;

        Iterable<Message> messageIterable = repoMessage.findTwoUsersConversation(conversationDTO.getUser1().getUserID(),conversationDTO.getUser2().getUserID());
        List<Message> allMessages = StreamSupport.stream(messageIterable.spliterator(),false)
                                    .sorted(Comparator.comparing(Message::getDate))
                                    .collect(Collectors.toList());
        for(Message message:allMessages){
            if(start  && message.getFrom().getId().equals(conversationDTO.getUser1().getUserID())){count++;}
            if(message.getId().equals(lastRead)){start = true;}
        }
        return count;
    }

    private String getUserPassword(String userID){
        if(userID == null)
            return null;
        String encryptedTextBase64 = "";
        try {
            encryptedTextBase64 = EncryptorAesGcmPassword.decrypt(srvUser.repo.getUsersEncryptedPassword(userID),srvUser.repo.getPasswordEncryptionPass());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedTextBase64;
    }

    @Override
    public void addObserver(Observer entity) {
        observers.add(entity);
    }

    @Override
    public void removeObserver(Observer entity) {
        observers.remove(entity);
    }

    @Override
    public void notifyObservers(Event entity) {
        if (observers != null){
            observers.forEach(x-> x.update(entity));
        }

    }
}


