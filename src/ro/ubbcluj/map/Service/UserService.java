package ro.ubbcluj.map.Service;

import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.myException.IDisAlreadyTakenException;
import ro.ubbcluj.map.repository.AppUserRepository;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.paging.Page;
import ro.ubbcluj.map.repository.paging.PageRequest;
import ro.ubbcluj.map.repository.paging.Pageable;
import ro.ubbcluj.map.repository.paging.PagedAppUserRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class UserService {
    //protected AppUserRepository<String, ApplicationUser> repo;
    protected PagedAppUserRepository<String,ApplicationUser> repo;
    public UserService(PagedAppUserRepository<String, ApplicationUser> repo) {
        this.repo = repo;
    }

    public ApplicationUser add(ApplicationUser user) throws IDisAlreadyTakenException {
        //user-ul este dummy validat in Service-ul din stratul superior
        if(user==null)
            throw  new IllegalArgumentException("User is not null\n");
            return (ApplicationUser) repo.save(user);
            //return user;
    }

    /**
     *
     * @param user
     * @return null -user has been successfully erased,ApplicationUser user otherwise
     */
    public ApplicationUser delete(ApplicationUser user){
        if(user==null)
            throw  new IllegalArgumentException("User can't be null\n");

        return (ApplicationUser) repo.delete(user);

    }


    public List<ApplicationUser> getAll(){
        List<ApplicationUser> allUsers = new ArrayList<>();
        Pageable current = new PageRequest(0,10);
        Page<ApplicationUser> applicationUserPage = repo.findAll(current);
        while (applicationUserPage != null) {
            allUsers.addAll(applicationUserPage.getContent().collect(Collectors.toList()));
            current = applicationUserPage.nextPageable();
            applicationUserPage = repo.findAll(current);
        }
       return allUsers;
    }

    /**
     *
     * @param idUser string User id
     * @param idFriend string Friend id
     * @return true if the users was successfully  added,false otherwise
     */
    public boolean addFriend(String idUser,String idFriend){
        ApplicationUser user=(ApplicationUser) this.repo.findOne(idUser);
        ApplicationUser userFriend=(ApplicationUser) this.repo.findOne(idFriend);
        if(user!=null&& userFriend!=null){
            user.addFriend(userFriend);
            return true;}
        return false;
    }
    /**
     *
     * @param idUser string User id
     * @param idFriend string Friend id
     * @return true if the users was successfully removed,false otherwise
     */
    public boolean deleteFriend(String idUser,String idFriend){
            ApplicationUser user=(ApplicationUser) this.repo.findOne(idUser);
            ApplicationUser userFriend=(ApplicationUser) this.repo.findOne(idFriend);
            if(user!=null&& userFriend!=null){
            user.removeFriend(userFriend);
            return true;}
            return false;
    }

    public ApplicationUser findOne(String userID){
        return (ApplicationUser) this.repo.findOne(userID);
    }


}
