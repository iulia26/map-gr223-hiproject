package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.Message;
import ro.ubbcluj.map.model.validators.ValidationException;

public interface MessageRepository<ID,E extends Entity<ID>>  {

  E saveMessage(E entity);
  E saveReplyMessage(ID originalId,E reply);
  Iterable<E> findUserMessages(String application_username);
  Iterable<E> findTwoUsersConversation(String user1,String user2);
  /**
   * @param id -the id of the entity to be returned
   *           id must not be null
   * @return the entity with the specified id
   * or null - if there is no entity with the given id
   * @throws IllegalArgumentException if id is null.
   */
  E findOne(ID id);

  /**
   * @return all entities
   */
  Iterable<E> findAll();
  Iterable<String> getUserConversationPartnersID(String userID);
  Long repliesTo(ID messageID);
}
