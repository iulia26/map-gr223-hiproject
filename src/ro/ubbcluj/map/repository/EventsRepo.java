package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.Event;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface EventsRepo<ID,E extends Entity<ID>> {

     boolean subscribedToEvent(Event event,String userID);

     /**
      *
      */
     List<E> findAll();

     List<E> findAllInASpecifiedDay(LocalDate date);
     /**
      * @param month - type Enum
      * @return allEvents that take place in a given month
      */
     List<E> findAllInASpecificMonthAndYear(LocalDate month);

     /**
      *
      * @param event
      * @return empty optional if the event was successfully saved,optional containing the event object otherwise
      */
     Optional<E> saveEvent(E event);

     /**
      *
      * @param userID String
      * @return empty optional if the user subscribed to the event,optional containing the userID otherwise
      */
     Optional<String> addInterestedUser(String userID, E event);

     /**
      *
      * @param userID
      * @return empty optional if the user unsubscribed to the event,optional containing the userID otherwise
      */
     Optional<String> removeUninterestedUser(String userID,E event);

     /**
      *
      */
     List<E> getUpcomingEventsForOneUser(ApplicationUser applicationUser);

     void cleanupPassedDueEvents();

}
