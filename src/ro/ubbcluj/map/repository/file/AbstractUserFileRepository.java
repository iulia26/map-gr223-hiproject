package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.repository.memory.InMemoryRepositoryUser;

import java.io.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @param <ID>  id data type
 * @param <E> entity
 */
public abstract  class AbstractUserFileRepository <ID,E extends Entity<ID>> extends InMemoryRepositoryUser<ID,E> {

    private String fileName;

    /**
     *
     * @param fileName file name in which we've saved data
     */
    public AbstractUserFileRepository(String fileName) {
        this.fileName = fileName;
        this.loadData();
    }

    /**
     *
     * @param entity E
     * @return null saved,entity otherwise
     */
    @Override
    public E save(E entity) {
        loadData();
        E element=super.save(entity);
        writeToFile();
        return element;
    }

    /**
     *
     * @param entity E
     * @return null updated,entity otherwise
     */
    @Override
    public E update(E entity) {
        loadData();
        E element= super.update(entity);
        writeToFile();
        return element;
    }

    /**
     *
     * @param entity E
     * @return null if entity was deleted and entity otherwise
     */
    @Override
    public E delete(E entity) {
        loadData();
        E element=super.delete(entity);
        writeToFile();
        return element;

    }

    /**
     * loads data from file fileName
     */
    protected void loadData(){
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            String line;
            while((line = bufferedReader.readLine()) != null){
                List<String> atributes = Arrays.asList(line.split(","));
                if(!atributes.isEmpty())
                {E element = extractEntity(atributes);
                    super.save(element);}
                //adaug in map lista de prieteni

            }
        }
        catch(FileNotFoundException e) {
            System.out.println("Fisierul nu a fost gasit!");
            e.printStackTrace();
        }
        catch(IOException e){
            System.out.println("Eroare la citire!");
            e.printStackTrace();
        }
    }

    /**
     * writes data to file
     */
    protected void writeToFile() {
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            Iterator<E> itr = this.findAll().iterator();
            while (itr.hasNext()) {
                E elem = itr.next();
                List<String> stringDataE = extractStringData(elem);
                fileWriter.write(String.join(",", stringDataE) + "\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error occured!");
            e.printStackTrace();

        }
    }

    /**
     *
     * @param attributes list string
     * @return entity form with attributes from list attributes
     */
    public abstract E extractEntity(List<String> attributes);

    /**
     *
     * @param entity E
     * @return string format of entity with attributes attributes
     */
    public abstract List<String> extractStringData(E entity);

}
