package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.model.ApplicationUser;

import java.util.ArrayList;
import java.util.List;

public class UserRepoFile extends AbstractUserFileRepository<String, ApplicationUser>{

    public UserRepoFile(String fileName) {
        super(fileName);
    }

    @Override
    public ApplicationUser extractEntity(List<String> attributes) {
        ApplicationUser entity=new ApplicationUser(attributes.get(0),attributes.get(1));
        entity.setId(attributes.get(2));
        entity.setIsValid(Boolean.parseBoolean(attributes.get(3)));
        return entity;
    }

    @Override
    public List<String> extractStringData(ApplicationUser entity) {
        List<String>data=new ArrayList<>();
        data.add(entity.getFirstName());
        data.add(entity.getLastName());
        data.add(entity.getId());
        data.add(Boolean.toString(entity.isValid()));
        return data;
    }

    @Override
    public ApplicationUser save(ApplicationUser entity) {
        this.loadData();
        ApplicationUser original=super.findOne(entity.getId());
        if(original!=null) {
            if ( original.isValid())
               return entity;
            else{
                original.setIsValid(true);//prietenia exista deja ,iar ea a fost reactivata doar
                this.writeToFile();
                return null;
            }
        }
        return super.save(entity);
    }

    @Override
    public ApplicationUser delete(ApplicationUser entity) {
        this.loadData();
        ApplicationUser original=this.findOne(entity.getId());
        if(original==null)
            return entity;
        original.setIsValid(false);
        this.writeToFile();
        //super.delete(entity);
        return null;
    }

    @Override
    public ApplicationUser update(ApplicationUser entity) {
        return super.update(entity);
    }

    @Override
    public Iterable<ApplicationUser> findAll() {
        this.loadData();
        return super.findAll();
    }

    @Override
    public ApplicationUser findOne(String s) {
        ApplicationUser original= super.findOne(s);
        if(original==null)return null;
        if(original.isValid())
            return original;
        return null;
    }
}
