package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.repository.memory.InMemoryRepositoryFriendship;

import java.io.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
//Il folosesc si pentru noua repo user

/**
 *
 * @param <ID> id entitate
 * @param <E> entity
 */
public abstract class AbstractFriendshipFileRepository<ID,E extends Entity<ID>> extends InMemoryRepositoryFriendship<ID,E> {

    private String fileName;

    /**
     *
     * @param fileName fisierul din care extragem datele
     */
    public AbstractFriendshipFileRepository(String fileName) {
        this.fileName = fileName;
        this.loadData();
    }

    /**
     *
     * @param entity entity must be not null
     * @return null if element was saved and false otherwise
     */
    @Override
    public E save(E entity) {
        loadData();
        E element=super.save(entity);
        writeToFile();
        return element;
    }

    /**
     *
     * @param entity entity must be not null
     * @return null if entity was updated and false otherwise
     */
    @Override
    public E update(E entity) {
        loadData();
        E element= super.update(entity);
        writeToFile();
        return element;
    }

    /**
     *
     * @param entity entity must be not null
     * @return null if the entity was deleted ,entity otherwise
     */
    @Override
    public E delete(E entity) {
        loadData();
        E element=super.delete(entity);
        writeToFile();
        return element;

    }

    /**
     * loads data from file
     */
    protected void loadData(){
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            String line;
            while((line = bufferedReader.readLine()) != null){
                List<String> attributes = Arrays.asList(line.split(","));
                if(!attributes.isEmpty())
                {E element = extractEntity(attributes);
                    super.save(element);}
                //adaug in map lista de prieteni

            }
        }
        catch(FileNotFoundException e) {
            System.out.println("Fisierul nu a fost gasit!");
            e.printStackTrace();
        }
        catch(IOException e){
            System.out.println("Eroare la citire!");
            e.printStackTrace();
        }
    }

    /**
     * stores data to file
     */
    protected void writeToFile() {
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            Iterator<E> itr = this.findAll().iterator();
            while (itr.hasNext()) {
                E elem = itr.next();
                List<String> stringDataE = extractStringData(elem);
                fileWriter.write(String.join(",", stringDataE) + "\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error occured!");
            e.printStackTrace();

        }
    }

    /**
     *
     * @param attributes list string
     * @return entitatea formata din datele date in lista attributes
     */
    public abstract E extractEntity(List<String> attributes);

    /**
     *
     * @param entity of type E
     * @return string representing entity
     */
    public abstract List<String> extractStringData(E entity);

}
