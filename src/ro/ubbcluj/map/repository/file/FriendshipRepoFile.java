package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.Tuple;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FriendshipRepoFile extends AbstractFriendshipFileRepository<Tuple<String,String>, Friendship>{

    public FriendshipRepoFile(String fileName) {
        super(fileName);
    }

    @Override
    public Friendship extractEntity(List<String> attributes) {
        List<Integer> date=new ArrayList<>();
        Arrays.asList(attributes.get(2).split("-")).forEach(elem->{date.add(Integer.parseInt(elem));});
        LocalDate localDate=LocalDate.of(date.get(0),date.get(1),date.get(2));
        Friendship entity= new Friendship(attributes.get(0),attributes.get(1),localDate);
        if(attributes.get(3).equals("true"))
            entity.setIsValid(true);
        else
            entity.setIsValid(false);
        return entity;

    }

    @Override
    public List<String> extractStringData(Friendship entity) {
        List<String> data=new ArrayList<>();
        data.add(entity.getUser1ID());
        data.add(entity.getUser2ID());
        data.add(entity.getStartDate().toString());
        if(entity.isValid()==true)
            data.add("true");
        else
            data.add("false");
        return data;

    }

    public Friendship save(Friendship friendship){

        this.loadData();
        Friendship original=super.findOne(friendship.getId());
        if(original!=null) {
            if ( original.isValid())
                throw new IllegalArgumentException("RepoError:Friendship already exists\n");
            else{
                original.setStartDate(friendship.getStartDate());
                original.setIsValid(true);//prietenia exista deja ,iar ea a fost reactivata doar
                this.writeToFile();
                return null;
            }
        }
        return super.save(friendship);//friendship saved

    }

    public Friendship delete(Friendship friendship){
        this.loadData();
        Friendship original=super.findOne(friendship.getId());
        if(original==null)
            return friendship;//nu exista aceasta prietenie
        original.setIsValid(false);
        super.delete(friendship);
        return null;
    }

    @Override
    public Iterable<Friendship> findAll() {
        this.loadData();
        return super.findAll();
    }

}
