package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.Entity;

public interface AppUserRepository <ID,E extends Entity<ID>> extends Repository<ID,E>{

    String getUsersEncryptedPassword(ID userID);
    String getPasswordEncryptionPass();
    boolean setUserPassword(String userPassword,String userID);
}
