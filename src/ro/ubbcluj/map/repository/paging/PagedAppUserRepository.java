package ro.ubbcluj.map.repository.paging;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.validators.ValidationException;

import java.util.List;

public interface PagedAppUserRepository <ID,E extends Entity<ID>>{

    String getUsersEncryptedPassword(ID userID);
    String getPasswordEncryptionPass();
    boolean setUserPassword(String userPassword,String userID);
    E findOne(ID id);
    /**
     * @return all entities
     */
    Page<E> findAll(Pageable pageable);
    Page<E> findAll(String startsWith,Pageable pageable);

    E save(E entity);
    E delete(E entity);
    E update(E entity);
}
