package ro.ubbcluj.map.repository.paging;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.SortingOrder;
import ro.ubbcluj.map.repository.Repository;

public interface PagedMessageRepository <ID, E extends Entity<ID>>{

    E saveMessage(E entity);
    E saveReplyMessage(ID originalId,E reply);

    Iterable<E> findUserMessages(String applicationUsername);
    Page<E> findTwoUsersConversation(String user1ID, String user2ID, SortingOrder order, Pageable pageable);
    /**
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     * or null - if there is no entity with the given id
     * @throws IllegalArgumentException if id is null.
     */
    E findOne(ID id);

    /**
     * @return all entities
     */
    Page<E> findAll(Pageable pageable);
    Page<String> getUserConversationPartnersID(String userID,Pageable pageable);
    Long repliesTo(ID messageID);

}
