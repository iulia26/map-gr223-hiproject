package ro.ubbcluj.map.repository.paging;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.Tuple;
import ro.ubbcluj.map.model.validators.ValidationException;

public interface PagedFriendshipRequestRepository<ID,E extends Entity<ID>> {

    /**
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     * or null - if there is no entity with the given id
     * @throws IllegalArgumentException if id is null.
     */
    E findOne(ID id);

    /**
     * @return all entities
     */
    Page<E> findAll(Pageable pageable);

    /**
     * @param entity entity must be not null
     * @return null- if the given entity is saved
     * otherwise returns the entity (id already exists)
     * @throws ValidationException      if the entity is not valid
     * @throws IllegalArgumentException if the given entity is null.     *
     */
    E save(E entity);
    E delete(E entity);
    E update(E entity);
    /**
     *
     * @param requestTuple Tuple<String,String> representing the id's of the two users
     *return FriendshipRequest id if these users have a pending friendship and null otherwise
     */
    ID findOnePendingRequestsByUserIdTuple(Tuple<String,String> requestTuple);
    Page<E> pendingFriendshipRequests(String applicationUser,Pageable pageable);
    int nrOfPendingFriendshipRequests(String applicationUser);
}
