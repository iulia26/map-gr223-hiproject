package ro.ubbcluj.map.repository.paging;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.validators.ValidationException;

public interface PagedFriendshipRepository<ID ,E extends Entity<ID>> {
    /**
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     * or null - if there is no entity with the given id
     * @throws IllegalArgumentException if id is null.
     */
    E findOne(ID id);

    /**
     * @return all entities
     */
    Page<E> findAll(Pageable pageable);

    /**
     * @param entity entity must be not null
     * @return null- if the given entity is saved
     * otherwise returns the entity (id already exists)
     * @throws ValidationException      if the entity is not valid
     * @throws IllegalArgumentException if the given entity is null.     *
     */
    E save(E entity);
    E delete(E entity);
    E update(E entity);
}
