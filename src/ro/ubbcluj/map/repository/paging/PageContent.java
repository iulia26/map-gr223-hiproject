package ro.ubbcluj.map.repository.paging;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PageContent<T> implements Page<T>{

    private Pageable pageable;
    private Stream<T> content;

    public PageContent(Pageable pageable, Stream<T> content) {
        this.pageable = pageable;
        this.content = content;
    }

    @Override
    public Pageable getPageable() {
        return this.pageable;
    }

    @Override
    public Pageable nextPageable() {
        return new PageRequest(pageable.getPageNumber() +1 , pageable.getPageSize());
    }

    @Override
    public Stream<T> getContent() {
        return this.content;
    }
}
