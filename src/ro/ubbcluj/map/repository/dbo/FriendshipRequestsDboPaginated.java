package ro.ubbcluj.map.repository.dbo;

import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.FriendshipRequest;
import ro.ubbcluj.map.model.FriendshipRequestStatus;
import ro.ubbcluj.map.model.Tuple;
import ro.ubbcluj.map.repository.paging.Page;
import ro.ubbcluj.map.repository.paging.PageContent;
import ro.ubbcluj.map.repository.paging.Pageable;
import ro.ubbcluj.map.repository.paging.PagedFriendshipRequestRepository;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FriendshipRequestsDboPaginated implements PagedFriendshipRequestRepository<Long, FriendshipRequest> {

    Connection connection;
    public FriendshipRequestsDboPaginated(Connection connection) {
        this.connection = connection;
    }


    /**
     *Looks for a  friendship request  where id  matches the @param id
     * @param id - Tuple<String,String> the first value of this tuple is expected to be the request sender's id
     * @return FriendshipRequest object with id = @param id ,null otherwise
     */
    @Override
    public FriendshipRequest findOne(Long id) {
        //in select adaug si data
        String query = "SELECT au.first_name as sender_first_name,au.last_name as sender_last_name,\n" +
                "fr.sender as sender_id,\n" +
                "au2.first_name as receiver_first_name,au2.last_name as receiver_last_name,\n" +
                "fr.status,fr.receiver as receiver_id,\n" +
                "fr.date as sending_date\n" +
                "FROM friendship_requests  fr\n" +
                "INNER JOIN application_users au on fr.sender = au.user_id\n" +
                "INNER JOIN application_users au2 on fr.receiver = au2.user_id\n" +
                //fr.data de adaugat in select
                "WHERE fr.id=?";
        try(
                PreparedStatement ps = connection.prepareStatement(query);
        ){
            ps.setLong(1,id);

            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                FriendshipRequest friendshipRequest = new FriendshipRequest(new ApplicationUser(rs.getString("sender_first_name"),rs.getString("sender_last_name"),rs.getString("sender_id"))
                        ,new ApplicationUser(rs.getString("receiver_first_name"),rs.getString("receiver_last_name"),rs.getString("receiver_id"))
                        ,id, rs.getTimestamp("sending_date").toLocalDateTime().toLocalDate());
                friendshipRequest.setStatus(FriendshipRequestStatus.getEnumValue(rs.getString("status")));

                rs.close();
                return  friendshipRequest;
            }
            rs.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    private List<FriendshipRequest> extractRequestsFromResultSet(ResultSet rs){
        List<FriendshipRequest> friendshipRequestList = new ArrayList<>();
        try {
            while (rs.next()) {
                ApplicationUser requestSender = new ApplicationUser(rs.getString("sender_first_name"), rs.getString("sender_last_name"), rs.getString("sender_id"));
                ApplicationUser requestReceiver = new ApplicationUser(rs.getString("receiver_first_name"), rs.getString("receiver_last_name"), rs.getString("receiver_id"));
                FriendshipRequest friendshipRequest = new FriendshipRequest(requestSender, requestReceiver
                        ,rs.getLong("request_id")
                        ,rs.getTimestamp("sending_date").toLocalDateTime().toLocalDate());
                String status = rs.getString("status");
                friendshipRequest.setStatus(FriendshipRequestStatus.getEnumValue(status));
                LocalDate date =rs.getTimestamp("sending_date").toLocalDateTime().toLocalDate();
                friendshipRequest.setDate(date);
                friendshipRequestList.add(friendshipRequest);
            }
            return friendshipRequestList;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return  null;
    }

    /**
     *Finds all pending friendship requests
     * @return Iterable<FriendshipRequests>
     *     */
    @Override
    public Page<FriendshipRequest> findAll(Pageable pageable) {
        String query = "SELECT au.first_name as sender_first_name,au.last_name as sender_last_name,\n" +
                "       fr.sender as sender_id,\n" +
                "       au2.first_name as receiver_first_name,au2.last_name as receiver_last_name,\n" +
                "       fr.receiver as receiver_id,\n" +
                "       fr.status,\n" +
                "       fr.id as request_id,\n" +
                "       fr.date as sending_date\n" +
                "FROM friendship_requests  fr\n" +
                "INNER JOIN application_users au on fr.sender = au.user_id\n" +
                "INNER JOIN application_users au2 on fr.receiver = au2.user_id WHERE fr.status='pending'" +
                "LIMIT ? OFFSET ?";
        try(PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1,pageable.getPageSize());
            ps.setInt(2,pageable.getPageSize() * pageable.getPageNumber());
            ResultSet rs = ps.executeQuery();
            List<FriendshipRequest> result = extractRequestsFromResultSet(rs);
            rs.close();
            if(result != null && result.size()>0)
                return new PageContent<FriendshipRequest>(pageable, result.stream());
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * saves a friendship request
     * @param entity entity must be not null,entity must have status equal to PENDING
     * @return null if the entity was saved,entity with id set if this friendshipRequest already existed and original entity otherwise
     */
    @Override
    public FriendshipRequest save(FriendshipRequest entity) {
        Long requestId = this.findOnePendingRequestsByUserIdTuple(new Tuple<>(entity.getFrom().getId(),entity.getTo().getId()) );
        if(requestId != null){
            entity.setId(requestId);
            return entity;
        }
        String query  = "INSERT INTO friendship_requests (sender,receiver,status,date) values (?,?,?,?)";
        try(
                PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1,entity.getFrom().getId());
            ps.setString(2,entity.getTo().getId());
            ps.setString(3,entity.getStatus().getStatusValue());
            ps.setTimestamp(4, Timestamp.valueOf(entity.getDate().atStartOfDay()));
            ps.executeUpdate();
            return null;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return entity;
    }
    /**
     *Deletes a friendshipRequest from the database
     * @param entity of type FriendshipRequest
     * @return null for successfully deleted entity and entity otherwise
     */
    @Override
    public FriendshipRequest delete(FriendshipRequest entity) {
        String query = "DELETE FROM friendship_requests  WHERE (id,sender,receiver,status) in ((?,?,?,?));";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setLong(1,entity.getId());
            ps.setString(2,entity.getFrom().getId());
            ps.setString(3,entity.getTo().getId());
            ps.setString(4,entity.getStatus().getStatusValue());
            ps.executeUpdate();
            return  null;
        }catch (SQLException e){
            e.printStackTrace();
            return entity;
        }
    }

    /**
     * updates friendship status
     * @param entity a friendship request
     * @return null if the entity was successfully updated ,entity otherwise
     */
    @Override
    public FriendshipRequest update(FriendshipRequest entity) {
        String query = "UPDATE friendship_requests SET status=? WHERE (sender,receiver,status) IN ((?,?,?),(?,?,?))";
        try(
                PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1,entity.getStatus().getStatusValue());
            ps.setString(2,entity.getFrom().getId());
            ps.setString(3,entity.getTo().getId());
            ps.setString(4,FriendshipRequestStatus.PENDING.getStatusValue());
            ps.setString(5,entity.getTo().getId());
            ps.setString(6,entity.getFrom().getId());
            ps.setString(7,FriendshipRequestStatus.PENDING.getStatusValue());
            ps.executeUpdate();
            return null;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return entity;
    }

    /**
     *
     * @param requestTuple Tuple<String,String> representing the id's of the two users
     *return FriendshipRequest id if these users have a pending friendship and null otherwise
     */
    @Override
    public Long findOnePendingRequestsByUserIdTuple(Tuple<String, String> requestTuple) {
        String query= "SELECT id FROM friendship_requests  WHERE status=? AND (sender,receiver) in ((?,?),(?,?))";
        try(
                PreparedStatement ps = connection.prepareStatement(query);
        ){ ps.setString(1,FriendshipRequestStatus.PENDING.getStatusValue());
            ps.setString(2,requestTuple.getFirst());
            ps.setString(3,requestTuple.getSecond());
            ps.setString(4,requestTuple.getSecond());
            ps.setString(5,requestTuple.getFirst());
            ResultSet rs = ps.executeQuery();
            if(rs.next())return rs.getLong("id");
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     *Retrieves one user's pending friendship requests
     * @param applicationUser String ,the user's e-mail
     * @return Iterable<FriendshipRequest>
     */
    @Override
    public Page<FriendshipRequest> pendingFriendshipRequests(String applicationUser,Pageable pageable) {
        String query = "SELECT au.first_name as sender_first_name,au.last_name as sender_last_name,\n" +
                "       fr.sender as sender_id,\n" +
                "       au2.first_name as receiver_first_name,au2.last_name as receiver_last_name,\n" +
                "       fr.receiver as receiver_id,\n" +
                "       fr.status,\n" +
                "       fr.id as request_id,\n" +
                "       fr.date as sending_date\n" +
                "FROM friendship_requests  fr\n" +
                "INNER JOIN application_users au on fr.sender = au.user_id\n" +
                "INNER JOIN application_users au2 on fr.receiver = au2.user_id WHERE fr.status=? AND fr.receiver=?" +
                "LIMIT ? OFFSET ?";
        try(
                PreparedStatement ps = connection.prepareStatement(query);
        ){  ps.setString(1,FriendshipRequestStatus.PENDING.getStatusValue());
            ps.setString(2,applicationUser);
            ps.setInt(3,pageable.getPageSize());
            ps.setInt(4,pageable.getPageSize() * pageable.getPageNumber());

            ResultSet rs = ps.executeQuery();
            List<FriendshipRequest> friendshipRequestList = extractRequestsFromResultSet(rs);
            rs.close();
            if(friendshipRequestList != null && friendshipRequestList.size()>0)
                return new PageContent<FriendshipRequest>(pageable,friendshipRequestList.stream());

        }catch (SQLException e){
            e.printStackTrace();
        }
        return  null;
    }

    @Override
    public int nrOfPendingFriendshipRequests(String applicationUser) {
        String query = "SELECT count(id) as requests FROM friendship_requests  WHERE status=? AND receiver=?";
        try(
                PreparedStatement ps = connection.prepareStatement(query);
        ){  ps.setString(1,FriendshipRequestStatus.PENDING.getStatusValue());
            ps.setString(2,applicationUser);
            ResultSet rs = ps.executeQuery();
            int count = 0;
            if(rs.next()){count =  rs.getInt("requests");}
            rs.close();
            return count;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
}
