package ro.ubbcluj.map.repository.dbo;

import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.Conversation;
import ro.ubbcluj.map.model.Tuple;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConversationDbo {
    private Connection connection;

    public ConversationDbo(Connection connection) {
        this.connection = connection;
    }

    /**
     *
     * @param id
     * @return Conversation if one with the specified id exists and null otherwise
     */
    public Conversation findOne(Long id){
        String query = "SELECT * FROM conversation_last_read WHERE id = ?";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setLong(1,id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return new Conversation(new ApplicationUser(null,null,rs.getString("user_1"))
                                        ,new ApplicationUser(null,null,rs.getString("user_2"))
                                        ,rs.getLong("last_read")
                                        ,rs.getLong("id"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
    /**
     *
     * @param usersInConversation
     * @return Conversation in one between the two users was found,null Otherwise
     */
    public Long findOnesID(Tuple<String,String> usersInConversation){
        String query = "SELECT id FROM conversation_last_read WHERE (user_1,user_2) IN ((?,?));";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1,usersInConversation.getFirst());
            ps.setString(2,usersInConversation.getSecond());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getLong("id");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param conversation
     * @return null if the Conversation was successfully updated,conversation otherwise
     */
    public Conversation update(Conversation conversation){
            String  query = "UPDATE conversation_last_read SET last_read = ? WHERE id = ?";
            try(PreparedStatement ps = connection.prepareStatement(query)){
                ps.setLong(1,conversation.getLastReadMessageID());
                ps.setLong(2,conversation.getConversationID());
                ps.executeUpdate();
                return null;
            }catch (SQLException e){
                e.printStackTrace();
            }
            return conversation;
    }

    /**
     *
     * @param conversation
     * @return null if the conversation was successfully saved,conversation otherwise
     */
    public Conversation save(Conversation conversation){
        Tuple<String,String> conversationMembers = new Tuple<String,String>(conversation.getUser1().getId(),conversation.getUser2().getId());
        if(findOnesID(conversationMembers) != null)return conversation;
        String query = "INSERT INTO conversation_last_read(user_1,user_2,last_read) VALUES (?,?,?)";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1,conversation.getUser1().getId());
            ps.setString(2,conversation.getUser2().getId());
            ps.setLong(3,conversation.getLastReadMessageID());
            ps.executeUpdate();
            return null;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return conversation;
    }
}
