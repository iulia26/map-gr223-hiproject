package ro.ubbcluj.map.repository.dbo;

import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.Event;
import ro.ubbcluj.map.repository.EventsRepo;

import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EventsRepoDbo implements EventsRepo<Long, Event> {
    Connection connection;

    public EventsRepoDbo(Connection connection) {
        this.connection = connection;
    }

    /**
     *
     * @param eventDate LocalDate
     * @return all events that take place in a specified date
     */
    @Override
    public List<Event> findAllInASpecifiedDay(LocalDate eventDate) {
        String query = "SELECT * FROM events WHERE settled_date = ?";
        List<Event> all = new ArrayList<>();
        try(PreparedStatement ps = connection.prepareStatement(query)){
                ps.setDate(1,Date.valueOf(eventDate.toString()));
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    all.add(new Event(rs.getString("description")
                                    ,rs.getDate("settled_date").toLocalDate()
                                    ,findOneEventSubscribers(rs.getLong("id"))
                                    ,rs.getLong("id")));
                }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return all;
    }

    @Override
    public List<Event> findAll() {
        String query = "SELECT * FROM events ;";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ResultSet rs = ps.executeQuery();
            List<Event> events = new ArrayList<>();
            while(rs.next()){
                events.add(new Event(rs.getString("description")
                        ,rs.getDate("settled_date").toLocalDate()
                        ,findOneEventSubscribers(rs.getLong("id"))
                        ,rs.getLong("id")));
            }
            if(events.size() == 0) return null;
            return events;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param userID
     * @param event
     * @return true,if the user is already subscribed to the event and false otherwise
     */
    @Override
    public boolean subscribedToEvent(Event event, String userID) {
        String checkIfAlreadyExists = "SELECT id FROM event_user WHERE event_id = ? and user_id = ? ";
        try(PreparedStatement ps = connection.prepareStatement(checkIfAlreadyExists)){
            ps.setLong(1,event.getId());
            ps.setString(2,userID);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) return true;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    private List<ApplicationUser> findOneEventSubscribers(Long id){
        List<ApplicationUser> subscribers = new ArrayList<>();
        String query = "SELECT user_id  FROM event_user\n" +
                "WHERE event_id = ?;";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setLong(1,id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                subscribers.add(new ApplicationUser(null,null,rs.getString("user_id")));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return subscribers;
    }

    /**
     *
     * @param yearMonth
     * @return all events in a specific month and year ordered by date
     */
    @Override
    public List<Event> findAllInASpecificMonthAndYear(LocalDate yearMonth) {
        String query = "SELECT * FROM events WHERE ? <= settled_date and settled_date <? ORDER BY settled_date;";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setDate(1, Date.valueOf(yearMonth.withDayOfMonth(1).toString()));
            ps.setDate(2,Date.valueOf(yearMonth.with(TemporalAdjusters.firstDayOfNextMonth())));
            ResultSet rs = ps.executeQuery();
            List<Event> events = new ArrayList<>();
            while(rs.next()){
                 events.add(new Event(rs.getString("description")
                                    ,rs.getDate("settled_date").toLocalDate()
                                    ,findOneEventSubscribers(rs.getLong("id"))
                                    ,rs.getLong("id")));
            }
            if(events.size() == 0) return null;
            return events;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Optional<Event> saveEvent(Event event) {
        String query = "INSERT INTO events(description,settled_date) VALUES (?,?)";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1,event.getDescription());
            ps.setDate(2,Date.valueOf(event.getEventDate().toString()));
            ps.executeUpdate();
            return Optional.empty();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.of(event);
    }


    private Optional<String> addRemoveSubscriber(String query,String userID,Event event,int flag){
        if(flag == 0){
            if(subscribedToEvent(event,userID)) return Optional.empty();
        }
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setLong(1,event.getId());
            ps.setString(2,userID);
            ps.executeUpdate();
            return Optional.empty();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.of(userID);
    }

    @Override
    public Optional<String> addInterestedUser(String userID,Event event) {

        String query = "INSERT INTO event_user(event_id,user_id) VALUES (?,?)" ;
       return addRemoveSubscriber(query,userID,event,0);
    }

    @Override
    public Optional<String> removeUninterestedUser(String userID,Event event) {

        String query = "DELETE FROM event_user WHERE event_id = ? and user_id = ?";
       return addRemoveSubscriber(query,userID,event,1);
    }

    /**
     *
     * @param applicationUser
     * @return list of all events a user participated at,the list is empty in there are no upcoming events for the specified user
     */
    @Override
    public List<Event> getUpcomingEventsForOneUser(ApplicationUser applicationUser) {
        String query = "select  distinct e.id,e.description,e.settled_date from events e\n" +
                "inner join\n" +
                "event_user eu on e.id = eu.event_id\n" +
                "WHERE  eu.user_id = ? and  (? <= e.settled_date  and e.settled_date <= ?)" +
                "order by settled_date asc;";
        List<Event> all = new ArrayList<>();
        try(PreparedStatement ps = connection.prepareStatement(query)){
                    ps.setString(1,applicationUser.getId());
                    LocalDate today = LocalDate.now();
                    ps.setDate(2,Date.valueOf(today.toString()));
                    LocalDate todayPlusTwo = today.plusDays(2L);
                    ps.setDate(3,Date.valueOf(todayPlusTwo.toString()));
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        all.add(new Event(rs.getString("description")
                                        ,rs.getDate("settled_date").toLocalDate()
                                        ,findOneEventSubscribers(rs.getLong("id"))
                                        ,rs.getLong("id")));
                    }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return all;
    }

    private List<Long> selectAllPassedDueEventsIDs(){
        String query = "SELECT id FROM events WHERE settled_date < ?";
        List<Long> trashEventsIDs = new ArrayList<>();
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setDate(1,Date.valueOf(LocalDate.now().toString()));
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                trashEventsIDs.add(rs.getLong("id"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return trashEventsIDs;
    }

    private void deleteEventIdOccurrencesInEventUserTable(Long eventId){
        String query = "DELETE FROM event_user WHERE event_id = ?";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setLong(1,eventId);
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    private void deleteEvent(Long eventID){
            String query = "DELETE FROM events WHERE id = ?";
            try(PreparedStatement ps = connection.prepareStatement(query)){
                ps.setLong(1,eventID);
                ps.executeUpdate();
            }catch (SQLException e){
                e.printStackTrace();
            }
    }
    @Override
    public void cleanupPassedDueEvents() {
        //select all passed due events id's
        //iterate this list and delete all this event id occurances from the event_user table
        //iterate and delete events from the events table
        List<Long> allTrashEventsIds = selectAllPassedDueEventsIDs();
        if(allTrashEventsIds.size() > 0){
            allTrashEventsIds.forEach(this::deleteEventIdOccurrencesInEventUserTable);
            allTrashEventsIds.forEach(this::deleteEvent);
        }

    }
}
