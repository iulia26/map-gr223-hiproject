package ro.ubbcluj.map.repository.dbo;
import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.Message;
import ro.ubbcluj.map.model.Tuple;
import ro.ubbcluj.map.myException.RepoError;
import ro.ubbcluj.map.repository.MessageRepository;
import ro.ubbcluj.map.repository.Repository;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageRepoDbo implements MessageRepository<Long,Message> {

    Connection connection;

    /**
     *
     * @param connection
     */
   public MessageRepoDbo(Connection connection) {
       this.connection = connection;
   }
    private LocalDateTime parseLocalDateTime(Timestamp timestamp){
        return timestamp.toLocalDateTime();
    }

    @Override
    public Long repliesTo(Long messageID) {
       String query = "SELECT m.original_message FROM messages m WHERE m.id = ?;";
        try (
                PreparedStatement statement = connection.prepareStatement(query);
        ) {
            statement.setLong(1,messageID);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                if(rs.getLong("original_message") > 0)
                    return rs.getLong("original_message");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     *Finds one message by id
     * @param messageID Long type
     * @return Message object with id = messageID
     */
    @Override
    public Message findOne(Long messageID) {
        String query = "select m0.id ,m0.sender as from_user_id,mr.recipient_id as to_user_id,m0.message,m0.date,\n" +
                "       user1.first_name as from_first_name,user1.last_name as from_last_name,\n" +
                "       user2.first_name as to_first_name,user2.last_name as to_last_name\n" +
                "from\n" +
                "messages m0 inner  join message_recipient mr on m0.id = mr.message_id\n" +
                "            inner join application_users user1 on user1.user_id=m0.sender\n" +
                "             inner join application_users user2 on user2.user_id=mr.recipient_id\n" +
                "where m0.id=? or m0.original_message=?\n" +
                "order by m0.date asc;";
        try (
             PreparedStatement statement = connection.prepareStatement(query);
        ) {
            statement.setLong(1, messageID);
            statement.setLong(2, messageID);
            ResultSet resultSet = statement.executeQuery();
            Optional<Message> message =Optional.empty();
            while (resultSet.next()) {
                Long currentMessageId = resultSet.getLong("id");
                String senderId = resultSet.getString("from_user_id");
                String senderFirstName = resultSet.getString("from_first_name");
                String senderLastName = resultSet.getString("from_last_name");
                String receiverId = resultSet.getString("to_user_id");
                String receiverFirstName = resultSet.getString("to_first_name");
                String receiverLastName = resultSet.getString("to_last_name");
                String text = resultSet.getString("message");
                LocalDateTime date = parseLocalDateTime(resultSet.getTimestamp("date"));

                if(currentMessageId.equals(messageID)) {
                    if (message.isEmpty()) {
                        message = Optional.of(new Message(new ApplicationUser(senderFirstName, senderLastName, senderId), new ArrayList<>(), text, date, messageID));
                    }
                    ApplicationUser recipient = new ApplicationUser(receiverFirstName, receiverLastName, receiverId);
                    message.get().addRecipient(recipient);
                }else
                        {
                            Message reply = new Message(new ApplicationUser(senderFirstName,senderLastName,senderId),new ArrayList<>(),text,date,currentMessageId);
                            message.ifPresent(value -> value.addReply(reply));
                        }

            }
            return message.orElse(null);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    private List<Message> fromResultSet(ResultSet resultSet) {
        Map<Long, Message> mapIdMessage = new HashMap<>();
        try{
        while (resultSet.next()) {
            Long currentMessageId = resultSet.getLong("id");

            String recipientId = resultSet.getString("to_user_id");
            String recipientFirstName = resultSet.getString("to_first_name");
            String recipientLatName = resultSet.getString("to_last_name");
            ApplicationUser messageRecipient = new ApplicationUser(recipientFirstName, recipientLatName, recipientId);

            String senderId = resultSet.getString("from_user_id");
            String senderFirstName = resultSet.getString("from_first_name");
            String senderLastName = resultSet.getString("from_last_name");
            ApplicationUser messageSender = new ApplicationUser(senderFirstName, senderLastName, senderId);

            String messageText = resultSet.getString("message");
            LocalDateTime date = parseLocalDateTime(resultSet.getTimestamp("date"));

            if (mapIdMessage.containsKey(currentMessageId)) {
                mapIdMessage.get(currentMessageId).addRecipient(messageRecipient);
            } else {
                Message message = new Message(messageSender, new ArrayList<>(), messageText, date, currentMessageId);
                message.addRecipient(messageRecipient);
                mapIdMessage.put(currentMessageId, message);
            }

            long repliesToId = resultSet.getLong("replies_to");
            if (repliesToId > 0) {
                Message reply = new Message(messageSender, new ArrayList<>(), messageText, date, currentMessageId);
                if(mapIdMessage.containsKey(repliesToId))
                    mapIdMessage.get(repliesToId).addReply(reply);

            }

            }
            return mapIdMessage.entrySet()
                .stream()
                .map(m -> {
                    Message message = new Message(m.getValue().getFrom(), m.getValue().getTo(), m.getValue().getMessage(), m.getValue().getDate(), m.getValue().getId());
                    message.setReply(m.getValue().getReply());
                    return message;
                })
                .collect(Collectors.toList());
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


    /**
     *Finds all messages between users from the social network database
     * @return Iterable<Message> where every Message components objects have only the id not null
     * */
    //paginare
    @Override
    public Iterable<Message> findAll()  {

        String sql = "select m0.id ,m0.original_message as replies_to,m0.sender as from_user_id,mr.recipient_id as to_user_id,m0.message,m0.date,\n" +
                     " user1.first_name as from_first_name,user1.last_name as from_last_name,\n" +
                     " user2.first_name as to_first_name,user2.last_name as to_last_name\n" +
                     "from\n" +
                     "    messages m0 inner  join message_recipient mr on m0.id = mr.message_id\n" +
                     "                inner join application_users user1 on user1.user_id=m0.sender\n" +
                     "                inner join application_users user2 on user2.user_id=mr.recipient_id\n" +
                     "order by m0.date asc;";
        try(
            PreparedStatement statement=connection.prepareStatement(sql);
            ResultSet resultSet=statement.executeQuery()){
            return fromResultSet(resultSet);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param entity entity must be not null
     * @return null for successfully added message,and Message object otherwise
     * Side effects: entity will receive a new id equal to the id given by the database
     */
    @Override
    public Message saveMessage(Message entity) {
        String query1 = "INSERT INTO messages(sender,message,date) VALUES (?,?,?)";
        String query2 = "INSERT INTO message_recipient(message_id,recipient_id) VALUES(?,?)";
        try(
            PreparedStatement ps = connection.prepareStatement(query1,PreparedStatement.RETURN_GENERATED_KEYS);
            PreparedStatement ps1 = connection.prepareStatement(query2);
            ){
            ps.setString(1,entity.getFrom().getId());
            ps.setString(2,entity.getMessage());
            ps.setTimestamp(3,Timestamp.valueOf(entity.getDate()));
            ps.executeUpdate();
            ResultSet resultSet = ps.getGeneratedKeys();
            resultSet.next();
            Long insertedMessageReceivedId = resultSet.getLong(1);
            resultSet.close();
            entity.setId(insertedMessageReceivedId);

            ps1.setLong(1,insertedMessageReceivedId);
            entity.getTo().forEach(recipient -> {
                try {
                    ps1.setString(2,recipient.getId());
                    ps1.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            return null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return entity;
        }
    }

    /**
     * Finds user's received messages
     * @param appUsername String user id (username)
     * @return Iterable<Message>
     */
    //paginare---nu merita,nu  folosesc funtia asta la gui
    @Override
    public Iterable<Message> findUserMessages(String appUsername) {
        String query = "select m0.id\n" +
                "from\n" +
                "    messages m0 inner join message_recipient mr on m0.id = mr.message_id\n" +
                "where mr.recipient_id=?\n" +
                "order by m0.date asc ;";
        try(
            PreparedStatement ps = connection.prepareStatement(query)
        ){
            ps.setString(1,appUsername);
            ResultSet rs = ps.executeQuery();
            List<Message> result = new ArrayList<>();
            while (rs.next()){
                result.add(findOne(rs.getLong("id")));
            }
            if(result !=null && result.size() > 0)
            {rs.close();
                return result;
            }
            rs.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }



    /**
     * Finds two users conversation history
     * @param user1 String ,first username
     * @param user2 String, second user username
     * @return Iterable<Message>
     */
    //paginare - aici ar fi cam 15 mesaje pe o pagina
    @Override
    public Iterable<Message> findTwoUsersConversation(String user1, String user2) {
        String query = "  select m0.id ,m0.original_message as replies_to,m0.sender as from_user_id,mr.recipient_id as to_user_id,m0.message,m0.date,\n" +
                "           user1.first_name as from_first_name,user1.last_name as from_last_name,\n" +
                "           user2.first_name as to_first_name,user2.last_name as to_last_name\n" +
                "    from\n" +
                "        messages m0 inner  join message_recipient mr on m0.id = mr.message_id\n" +
                "                    inner join application_users user1 on user1.user_id=m0.sender\n" +
                "                    inner join application_users user2 on user2.user_id=mr.recipient_id\n" +
                "    where (mr.recipient_id,m0.sender) in ((?,?),(?,?))\n" +
                "    order by m0.date asc ;";
        try(
            PreparedStatement ps = connection.prepareStatement(query)
        ){
          ps.setString(1,user1);
          ps.setString(2,user2);
          ps.setString(3,user2);
          ps.setString(4,user1);
          ResultSet rs = ps.executeQuery();
          List<Message> result = fromResultSet(rs);
          if(result != null && result.size()>0){
              rs.close();
              return result;
          }
          rs.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Saves a message reply
     * @param originalMessageId Long,the original message id
     * @param reply Message - reply for message with id originalMessageId
     * @return
     */
    @Override
    public Message saveReplyMessage(Long originalMessageId,Message reply) {
        String query1 = "INSERT INTO messages(sender,message,date,original_message) VALUES (?,?,?,?)";
        String query2 = "INSERT INTO message_recipient(message_id,recipient_id) VALUES(?,?)";
        try(
            PreparedStatement ps = connection.prepareStatement(query1,PreparedStatement.RETURN_GENERATED_KEYS);
            PreparedStatement ps1 = connection.prepareStatement(query2);
        ){
            ps.setString(1,reply.getFrom().getId());
            ps.setString(2,reply.getMessage());
            ps.setTimestamp(3,Timestamp.valueOf(reply.getDate()));
            ps.setLong(4,originalMessageId);
            ps.executeUpdate();
            ResultSet resultSet = ps.getGeneratedKeys();
            resultSet.next();
            Long insertedMessageReceivedId = resultSet.getLong(1);
            resultSet.close();
            reply.setId(insertedMessageReceivedId);

            ps1.setLong(1,insertedMessageReceivedId);
            reply.getTo().forEach(recipient -> {
                try {
                    ps1.setString(2,recipient.getId());
                    ps1.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            return null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return reply;
        }
    }

    /**
     *Get a list of all  users that one  has ever had a conversation with
     * @param userID - a valid user id of type String
     * @return a list of iterable Strings representing the id's of the application users or a list with no elements if the user did not participate in any conversation if the operation succeeded
     * and null otherwise
     */
    //aici poate o paginare ca sa ma ajute la view
    @Override
    public Iterable<String> getUserConversationPartnersID(String userID) {
        String query = "SELECT DISTINCT\n" +
                        "CASE\n" +
                        "WHEN m.sender = ? THEN mr.recipient_id\n" +
                        "ELSE m.sender\n" +
                        "END\n" +
                        "AS chat_partner_id\n" +
                        "FROM messages  m inner join message_recipient mr on m.id = mr.message_id " +
                        "AND\n" +
                        "( m.sender = ? OR mr.recipient_id = ?);";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1,userID);
            ps.setString(2,userID);
            ps.setString(3,userID);
            ResultSet rs = ps.executeQuery();
            List<String> usersID = new ArrayList<>();
            while(rs.next()){
               usersID.add(rs.getString("chat_partner_id"));
            }
            return usersID;
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }


}
