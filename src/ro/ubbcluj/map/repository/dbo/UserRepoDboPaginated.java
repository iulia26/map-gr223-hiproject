package ro.ubbcluj.map.repository.dbo;

import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.repository.paging.Page;
import ro.ubbcluj.map.repository.paging.PageContent;
import ro.ubbcluj.map.repository.paging.Pageable;
import ro.ubbcluj.map.repository.paging.PagedAppUserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepoDboPaginated implements PagedAppUserRepository<String, ApplicationUser> {
    Connection connection;
    private String passwordEncryptionPass = "network26A@";

    public UserRepoDboPaginated(Connection connection) {
        this.connection = connection;
    }
    /**
     *
     * @param id String
     * @return one user with specified id even if the user has been softly deleted
     */
    private ApplicationUser findOneEvenIfNotValid(String id) {
        String sql="select * from application_users where user_id=?";
        try(PreparedStatement ps=connection.prepareStatement(sql);
        ){
            ps.setString(1,id);
            ResultSet resultSet=ps.executeQuery();
            if (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String userId = resultSet.getString("user_id");
                Boolean isValid=resultSet.getBoolean("is_valid");
                ApplicationUser user = new ApplicationUser(firstName, lastName);
                user.setIsValid(isValid);
                user.setId(userId);
                resultSet.close();
                return user;

            }
            resultSet.close();
            return null;
        }catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     *
     * @param userID
     * @return user's encrypted password if the user exists and null otherwise
     */
    @Override
    public String getUsersEncryptedPassword(String userID) {
        String query = "SELECT coded_password FROM application_users WHERE user_id = ?";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1,userID);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getString("coded_password");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getPasswordEncryptionPass() {
        return passwordEncryptionPass;
    }

    @Override
    public boolean setUserPassword(String userCodedPassword, String userID) {
        String query = "UPDATE application_users SET coded_password = ? WHERE user_id = ?";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1,userCodedPassword);
            ps.setString(2,userID);
            ps.executeUpdate();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     *
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return valid entity if id is valid,null otherwise
     */
    @Override
    public ApplicationUser findOne(String id) {
        String sql="select * from application_users where (user_id,is_valid) in ((?,true))";
        try(
                PreparedStatement ps=connection.prepareStatement(sql);
        ){
            ps.setString(1,id);
            ResultSet resultSet=ps.executeQuery();
            if (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String userId = resultSet.getString("user_id");
                ApplicationUser user = new ApplicationUser(firstName, lastName);
                user.setIsValid(true);
                user.setId(userId);
                resultSet.close();
                return user;

            }
            resultSet.close();
            return null;
        }catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @return  all entities present in the dbo
     */
    @Override
    public Page<ApplicationUser> findAll(Pageable pageable) {
        List<ApplicationUser> users=new ArrayList<>();
        String sql="SELECT * FROM application_users  LIMIT ? OFFSET ?";
        try(
                PreparedStatement statement=connection.prepareStatement(sql);
                ){
            statement.setInt(1,pageable.getPageSize());
            statement.setInt(2,pageable.getPageSize()*pageable.getPageNumber());
            ResultSet resultSet=statement.executeQuery();
            while (resultSet.next()){
                String firstName=resultSet.getString("first_name");
                String lastName=resultSet.getString("last_name");
                String userId=resultSet.getString("user_id");
                Boolean isValid=resultSet.getBoolean("is_valid");
                ApplicationUser user=new ApplicationUser(firstName,lastName);
                user.setId(userId);
                user.setIsValid(isValid);
                users.add(user);
            }
            resultSet.close();
            if(users.size() > 0)
                return new PageContent<ApplicationUser>(pageable,users.stream());
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Page<ApplicationUser> findAll(String startsWith,Pageable pageable) {
        List<ApplicationUser> users=new ArrayList<>();
        String sql="SELECT * FROM application_users where first_name LIKE ? OR last_name LIKE ? LIMIT ? OFFSET ?";
        try(
                PreparedStatement statement=connection.prepareStatement(sql);
        ){
            statement.setString(1,startsWith+"%");
            statement.setString(2,startsWith+"%");
            statement.setInt(3,pageable.getPageSize());
            statement.setInt(4,pageable.getPageSize()*pageable.getPageNumber());
            ResultSet resultSet=statement.executeQuery();
            while (resultSet.next()){
                String firstName=resultSet.getString("first_name");
                String lastName=resultSet.getString("last_name");
                String userId=resultSet.getString("user_id");
                Boolean isValid=resultSet.getBoolean("is_valid");
                ApplicationUser user=new ApplicationUser(firstName,lastName);
                user.setId(userId);
                user.setIsValid(isValid);
                users.add(user);
            }
            resultSet.close();
            if(users.size() > 0)
                return new PageContent<ApplicationUser>(pageable,users.stream());
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *save entity
     * @param entity entity must be not null
     * @return null successfully saved entity,entity otherwise
     */
    @Override
    public ApplicationUser save(ApplicationUser entity) {
        ApplicationUser original=this.findOneEvenIfNotValid(entity.getId());
        if(original==null) {
            String sql = "insert into application_users (first_name,last_name,user_id,is_valid) values (?,?,?,true)";
            try (
                    PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setString(1, entity.getFirstName());
                ps.setString(2, entity.getLastName());
                ps.setString(3, entity.getId());
                ps.executeUpdate();
                return null;
            } catch (SQLException e) {
                return entity;
            }
        }
        else {
            if(original.isValid())return entity;
            entity.setIsValid(true);//daca s-a ajuns aici atunci e foarte probabil ca entiatea sa existe si atunci actualizez validitatea
            return update(entity);
        }
    }
    /**
     * Softly delete entity
     * @param entity E
     * @return null successfully soft deleted entity,entity otherwise
     */
    @Override
    public ApplicationUser delete(ApplicationUser entity) {
        ApplicationUser original=this.findOneEvenIfNotValid(entity.getId());
        if(original==null)return entity;
        if(!original.isValid()) return entity;
        String sql="UPDATE application_users set is_valid=false where user_id=?";
        try(
                PreparedStatement ps=connection.prepareStatement(sql))   {
            ps.setString(1,entity.getId());
            ps.executeUpdate();
            return null;
        }catch (SQLException e) {
            return entity;
        }
    }
    /**
     *
     * @param entity E
     * @return null for successfully updated user and entity otherwise
     */
    @Override
    public ApplicationUser update(ApplicationUser entity) {
        String sql="UPDATE application_users SET first_name=?,last_name=?,is_valid=? WHERE user_id=? ";
        try(
                PreparedStatement ps=connection.prepareStatement(sql))   {
            ps.setString(1,entity.getFirstName());
            ps.setString(2,entity.getLastName());
            ps.setBoolean(3,entity.isValid());
            ps.setString(4,entity.getId());
            ps.executeUpdate();
            return null;
        }catch (SQLException e) {
            return entity;
        }
    }
}
