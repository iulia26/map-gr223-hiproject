package ro.ubbcluj.map.repository.dbo;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.Tuple;
import ro.ubbcluj.map.model.validators.Validator;
import ro.ubbcluj.map.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Database repository for Friendship Relationships
 */
public class FriendshipRepoDbo implements Repository<Tuple<String,String>, Friendship> {


    private Validator<Friendship> validator;
    private Connection connection;

    /**
     *
     * @param connection driver management connection
     * @param validator
     */
    public FriendshipRepoDbo(Connection connection, Validator<Friendship> validator) {

            this.connection =  connection;
            this.validator = validator;
    }

    /**
     *
     * @param stringDate string that represents current date separated by "-" character ,eg."2021-03-21" we have year followed by month and day of the month
     * @return LocalDate
     */
    private LocalDate getLocalDateFromString(String stringDate){
        List<Integer> dateValues=new ArrayList<>();
        Arrays.asList(stringDate.split("-")).forEach(elem->{dateValues.add(Integer.parseInt(elem));});
        LocalDate localDate=LocalDate.of(dateValues.get(0),dateValues.get(1),dateValues.get(2));
        return localDate;
    }

    /**
     *Compares two strings
     * @param str1 String
     * @param str2 String
     * @return minimum of the two strings
     * ex.for "aBa" and "aNa" the minimum would be "aBa"
     */
    private String minString(String str1,String str2){
        if(str1.compareTo(str2)<0)
            return str1;
        return str2;
    }

    /**
     *Compares two strings
     * @param str1 String
     * @param str2 String
     * @return maximum of the two strings
     * ex.for "aBa" and  "aNa" the maximum would be "aNa"
     */
    private String maxString(String str1,String str2){
        if(str1.compareTo(str2)<0)
            return str2;
        return str1;
    }

    /**
     *In a soft delete implementations returns all entities even if they are still valid(true) or not(false)
     * @param stringStringTuple Tuple<String,String>
     * @return all friendship entities existent in the database
     */
    private Friendship findOneEvenIfNotValid(Tuple<String, String> stringStringTuple){
        String sql = "select * from friendships where (user1_id,user2_id) in ((?,?),(?,?))";
        try(PreparedStatement ps=connection.prepareStatement(sql)){
            String first=stringStringTuple.getFirst();
            String second=stringStringTuple.getSecond();
            ps.setString(1, first);
            ps.setString(2, second);
            ps.setString(3,second);
            ps.setString(4,first);
            //ps.executeUpdate();
            ResultSet resultSet =ps.executeQuery();
            if(resultSet.next()){
                String user1ID=resultSet.getString("user1_id");
                String user2ID=resultSet.getString("user2_id");
                Boolean isValid=resultSet.getBoolean("is_valid");
                Friendship friendship=new Friendship(user1ID,user2ID,getLocalDateFromString(resultSet.getString("start_date")));
                friendship.setIsValid(isValid); resultSet.close();
                return friendship;

            }
            return null;
        }catch (SQLException e){

            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param stringStringTuple Tuple<String, String>
     * @return Friendship object if this object exists in the dbo and is valid,
     * null otherwise
     */
    @Override
    public Friendship findOne(Tuple<String, String> stringStringTuple) {
        String sql = "select * from  friendships where (user1_id,user2_id,is_valid) in ((?,?,true),(?,?,true))";
        try(PreparedStatement ps=connection.prepareStatement(sql)){
                String first=stringStringTuple.getFirst();
                String second=stringStringTuple.getSecond();
                ps.setString(1, first);
                ps.setString(2, second);
                ps.setString(3,second);
                 ps.setString(4,first);
                //ps.executeUpdate();
            ResultSet resultSet =ps.executeQuery();
            if(resultSet.next()){
                String user1ID=resultSet.getString("user1_id");
                String user2ID=resultSet.getString("user2_id");
                Boolean isValid=resultSet.getBoolean("is_valid");
                Friendship friendship=new Friendship(user1ID,user2ID,getLocalDateFromString(resultSet.getString("start_date")));
                friendship.setIsValid(isValid); resultSet.close();
                return friendship;

            }
            return null;
        }catch (SQLException e){

            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @return all Friendship entities found in the dbo even if valid or not
     */
    //paginare
    @Override
    public Iterable<Friendship> findAll() {
        List<Friendship> friendships=new ArrayList<>();
        try(
            PreparedStatement statement=connection.prepareStatement("SELECT * FROM friendships");
            ResultSet resultSet =statement.executeQuery()){
            while (resultSet.next()){
                String user1ID=resultSet.getString("user1_id");
                String user2ID=resultSet.getString("user2_id");
                Boolean isValid=resultSet.getBoolean("is_valid");
                Friendship friendship=new Friendship(user1ID,user2ID,getLocalDateFromString(resultSet.getString("start_date")));
                friendship.setIsValid(isValid);
                friendships.add(friendship);
            }
            return friendships;
        }catch (SQLException e){
            e.printStackTrace();
            return friendships;
        }

    }


    @Override
    public Friendship save(Friendship entity) {
        Friendship original=this.findOneEvenIfNotValid(entity.getId());
            if(original==null) {
                String sql = "insert into friendships (user1_id,user2_id,start_date,is_valid) values (?,?,?,true)";
                try (
                     PreparedStatement ps = connection.prepareStatement(sql)) {

                    String first = entity.getUser1ID();
                    String second = entity.getUser2ID();
                    ps.setString(1, this.minString(first, second));
                    ps.setString(2, this.maxString(first, second));
                    ps.setString(3, entity.getStartDate().toString());
                    ps.executeUpdate();

                    return null;

                } catch (SQLException e) {
                    return entity;
                }
            }
            else
                if(original.isValid())
                    return entity;
                entity.setIsValid(true);
                return this.update(entity);

        }
    /**
     *
     * @param entity E
     * @return entity when failed to update entity,null otherwise
     */
    @Override
    public Friendship update(Friendship entity) {
            String sql="UPDATE friendships SET start_date=?,is_valid=? WHERE (user1_id,user2_id) in ((?,?),(?,?)) ";
            try(
                PreparedStatement ps=connection.prepareStatement(sql))   {
                String first=entity.getUser1ID();
                String second=entity.getUser2ID();
                ps.setString(1,entity.getStartDate().toString());
                ps.setBoolean(2,entity.isValid());
                ps.setString(3, first);
                ps.setString(4, second);
                ps.setString(5,second);
                ps.setString(6,first);
                ps.executeUpdate();
                return null;
            }catch (SQLException e) {
                //this.storeExceptionSomewhereElse(e);
                return entity;
            }

    }

    @Override
    public Friendship delete(Friendship entity) {
        Friendship original=this.findOneEvenIfNotValid(entity.getId());
        if(original==null)return entity;//doesn't exist
        if(!original.isValid())return entity;//exists,but not active

        String sql="UPDATE friendships SET is_valid=false WHERE (user1_id,user2_id) in ((?,?),(?,?)) ";
        try(
            PreparedStatement ps=connection.prepareStatement(sql))   {
            String first=entity.getUser1ID();
            String second=entity.getUser2ID();
            ps.setString(1, first);
            ps.setString(2, second);
            ps.setString(3,second);
            ps.setString(4,first);
            ps.executeUpdate();

            return null;
        }catch (SQLException e) {
            return entity;
        }
    }
}
