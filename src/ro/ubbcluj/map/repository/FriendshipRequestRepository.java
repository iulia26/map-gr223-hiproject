package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.Tuple;

import java.sql.Connection;

public interface FriendshipRequestRepository<ID,E extends Entity<ID>> extends Repository<ID,E>{

    /**
     *
     * @param requestTuple Tuple<String,String> representing the id's of the two users
     *return FriendshipRequest id if these users have a pending friendship and null otherwise
     */
     ID findOnePendingRequestsByUserIdTuple(Tuple<String,String> requestTuple);
     Iterable<E> pendingFriendshipRequests(String application_user);
     Iterable<E> findAllDespiteStatus(String applicationUser);
}
