package ro.ubbcluj.map.repository.memory;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepositoryFriendship <ID,E extends Entity<ID>>implements Repository<ID,E> {
    private Map<ID,E> entities;

    public InMemoryRepositoryFriendship() {
        this.entities = new HashMap<>();
    }

    @Override
    public E findOne(ID id) {
        if(id==null)
            throw new IllegalArgumentException("ID mustn't be null");
        return entities.get(id);
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     *
     * @param entity entity must be not null
     * @return null if the element was saved,entity otherwise
     */
    @Override
    public E save(E entity) {
        if(entity==null)
            throw new IllegalArgumentException("Id null");
        if(entities.get(entity.getId())!=null)
            return entity;
        entities.put(entity.getId(),entity);
        return null;
    }
    /**
     *
     * @param entity entity must be not null
     * @return null if the element was updated,entity otherwise
     *
     */
    @Override
    public E update(E entity) {
        if(entity ==null)
            throw new IllegalArgumentException("entity must not be null");
        E newEntity=entities.computeIfPresent(entity.getId(),(k,v) -> entity);
        if(newEntity!=null)//inseamna ca entitatea a fost actualizata
            return null;
        return entity;
    }

    /**
     *
     * @param entity entity must be not null
     * @return null if the element was deleted,entity otherwise
     */
    @Override
    public E delete(E entity) {
        /*if(entity==null)
            throw new IllegalArgumentException("Id null");
        if(entities.get(entity.getId())==null)
            return entity;
        this.entities.remove(entity.getId());*/
        return null;
    }


}
