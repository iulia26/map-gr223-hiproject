package ro.ubbcluj.map.repository.memory;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepositoryUser<ID,E extends Entity<ID>> implements Repository<ID,E> {
    private Map<ID,E> entities;

    public InMemoryRepositoryUser() {
        this.entities = new HashMap<>();
    }

    @Override
    public E findOne(ID id) {
        if(id==null)
            throw new IllegalArgumentException("ID mustn't be null");
        return entities.get(id);
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public E save(E entity) {
        if(entity==null)
            throw new IllegalArgumentException("Id null");

        if(entities.get(entity.getId())!=null)
            return entity;
        entities.put(entity.getId(),entity);
        return null;
    }

    @Override
    public E update(E entity) {
        if(entity ==null)
            throw new IllegalArgumentException("entity must not be null");
        E newEntity=entities.computeIfPresent(entity.getId(),(k,v) -> entity);
        if(newEntity!=null)//inseamna ca entitatea a fost actualizata
            return null;
        return entity;
    }

    @Override
    public E delete(E entity) {
        if(entity==null)
            throw new IllegalArgumentException("Id null");
        if(entities.get(entity.getId())==null)
            return entity;
        this.entities.remove(entity.getId());
        return null;
    }
}
