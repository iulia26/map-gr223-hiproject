package ro.ubbcluj.map.utils.events;

public enum EventType {

    MESSAGE,
    FRIENDSHIP,
    FriendshipRequests,
    EVENTS_SUBSCRIPTION;


    private EventType() {
    }
}
