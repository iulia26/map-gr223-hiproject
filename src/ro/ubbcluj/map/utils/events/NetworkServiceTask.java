package ro.ubbcluj.map.utils.events;

public class NetworkServiceTask implements Event{
    EventType eventType;

    public NetworkServiceTask(EventType eventType) {
        this.eventType = eventType;
    }

    public EventType getEventType() {
        return eventType;
    }
}
