package ro.ubbcluj.map.utils.observer;

import ro.ubbcluj.map.utils.events.Event;

public interface Observable <E extends Event>{
    void addObserver(Observer<E> entity);
    void removeObserver(Observer<E> entity);
    void notifyObservers(E entity);
}
