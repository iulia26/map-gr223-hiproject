package ro.ubbcluj.map.ui;

import ro.ubbcluj.map.Service.NetworkService;

import ro.ubbcluj.map.model.*;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.myException.IDisAlreadyTakenException;
import ro.ubbcluj.map.myException.InsufficientDataToExecuteTaskException;
import ro.ubbcluj.map.myException.RepoError;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * consola
 */
public class Console {
    private NetworkService srv;
    private Scanner input;

    /**
     *
     * @param srv of type NetworkService
     */
    public Console(NetworkService srv) {
        this.srv = srv;
        this.input = new Scanner(System.in);
    }

    /**
     * add user
     */
    public void addUser(){

        System.out.print("First name:");
        String firstName=input.nextLine();
        System.out.print("Last name:");
        String lastName=input.nextLine();
        System.out.print("Enter user's e-mail:");
        String mail=input.nextLine();
        try {
            if(srv.addUser(new UserDto<String>(mail,firstName,lastName),"parolaUser")==null)
                System.out.println("Successfully registered user!\n");
        } catch (IDisAlreadyTakenException | ValidationException | IllegalArgumentException a) {
            System.out.println(a.getMessage());
        }

    }

    /**
     * add friend
     */
    public void addFriend(){
        System.out.println("First user ID:");
        String id1=input.nextLine();
        System.out.println("Second user ID:");
        String id2=input.nextLine();
        try {
            if(this.srv.addFriendship(id1,id2))
            System.out.println("Successfully added friendship!\n");
            else System.out.println("Couldn't add friendship");
        } catch (InsufficientDataToExecuteTaskException | IllegalArgumentException | ValidationException e) {
            System.out.println(e.getMessage());
        }


    }

    /**
     * delete user
     */
    public void removeUser(){
        System.out.println("User to be removed?(enter ID):");
        String id=input.nextLine();
        try{
            if(srv.deleteUser(id)==null)
            System.out.println("Successfully removed user!\n");
        }catch(ValidationException | IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

    }

    /**
     * delete friend
     */
    public void removeFriend(){
        System.out.println("Enter user's ID:");
        String idUser=input.nextLine();
        System.out.println("Enter user's friend ID:");
        String idFriend=input.nextLine();
        try{
            if(srv.deleteFriendship(idUser,idFriend)==null){
                System.out.println("Successfully removed friend!\n");
            }
        }catch(ValidationException | IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * update user
     */
    public void updateUser(){

    }

    /**
     * count connected components
     */
    public void countCommunities(){
        System.out.print("Number of formed communities:");
        System.out.println(srv.communitiesCounter());
    }

    /**
     * frendliest community  members
     */
    public void friendliestCommunity(){
        try {
            System.out.println(srv.friendliestCommunity());
        } catch (InsufficientDataToExecuteTaskException e) {
            e.getMessage();
        }
    }

    /**
     * print all users
     */
    public void getAllUsers(){
        List<UserDto<String>> all=srv.getAllUsers();
        for(UserDto<String> user:all){
            System.out.println(user);
        }
    }


    /**
     * print user's friends by id
     */
    private void getUserFriendships() {
        System.out.println("Enter user's ID:");
        String idUser=input.nextLine();
        List<FriendshipDto<String>> allFriendships= new ArrayList<>();
        try {
            allFriendships = this.srv.getFriendshipList(idUser);
            if(allFriendships.size()==0)
                System.out.println("This user doesn't have any friends.\n");
            else{
                for(FriendshipDto<String> elem:allFriendships) {
                    if (elem.getUser1().getUserID().equals(idUser))
                    System.out.println(elem.getUser2().getFirstName() + " " + elem.getUser2().getLastName() + " " + elem.getDate());
                    else System.out.println(elem.getUser1().getFirstName() + " " + elem.getUser1().getLastName() + " " + elem.getDate());
                }
            }
        } catch (InsufficientDataToExecuteTaskException | RepoError e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * print user's friends by id and month
     */
    private void getUserAllFriendsByDate() {
        System.out.println("Enter user's ID:");
        String idUser=input.nextLine();

        System.out.println("Enter friendship's month:");
        int month=Integer.parseInt(input.nextLine());

        List<FriendshipDto<String>> allFrienships= new ArrayList<>();
        try {
            allFrienships = this.srv.getFriendshipListByDate(idUser,month);
            if(allFrienships.size()==0)
                System.out.println("This user doesn't have any new friends on that date.\n");
            else{
                for(FriendshipDto<String> elem:allFrienships){
                    if (elem.getUser1().getUserID().equals(idUser))
                    System.out.println(elem.getUser2().getFirstName()+" "+elem.getUser2().getLastName()+" "+elem.getDate());
                    else System.out.println(elem.getUser1().getFirstName()+" "+elem.getUser1().getLastName()+" "+elem.getDate());
                }
            }
        } catch (InsufficientDataToExecuteTaskException | RepoError e) {
            System.out.println(e.getMessage());
        }
    }

    public void conversationHistory(){
        System.out.println("Enter users ID\n");
        System.out.println("User 1:");
        String idUser1=input.nextLine();
        System.out.println("User 2:");
        String idUser2=input.nextLine();
        try {
            List<MessageDTO> all = srv.getConversationHistory(idUser1, idUser2);
            if (all.size() == 0) System.out.println("Couldn't find any conversation with this combination.");
            else {
                for (MessageDTO message : all)
                    System.out.println(message);
            }
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }

    public void replyMessage() {

            System.out.print("Enter user's E-mail:");
            String username = input.nextLine();
            if(!srv.logIN(username,"parolaUser")) System.out.println("Incorrect user E-mail!");
            else {
                try {
                    Boolean keepGoing = true;
                    while (keepGoing) {
                        List<MessageDTO> messagesNotRepliedYet = srv.getUserReceivedMessages(username);

                        if (messagesNotRepliedYet.size() > 0) {
                            int index = 0;
                            for (MessageDTO messageDTO : messagesNotRepliedYet) {
                                System.out.println(String.valueOf(index) + " " + messageDTO.toString());
                                index++;
                            }
                            String next = "What do you wish to do next?\nReply to one message:w\nReply all:wa\nTo close session write:q\n";

                            System.out.print(next);
                            String command = input.nextLine();
                            if (command.equals("w") || command.equals("wa")) {
                                System.out.print("Choose a message number:");
                                index = Integer.parseInt(input.nextLine());
                                System.out.print("Message reply:");
                                String msgReply = input.nextLine();
                                if (command.equals("w")) {
                                    MessageDTO messageNotRepliedYet = messagesNotRepliedYet.get(index);
                                    Optional<UserDto<String>> sender = messageNotRepliedYet.getTo().stream().filter(stringUserDto -> stringUserDto.getUserID().equals(username)).findFirst();
                                    MessageDTO reply = new MessageDTO(sender.get()
                                                                    ,List.of(messageNotRepliedYet.getFrom())
                                                                    ,msgReply,LocalDateTime.now(),0L);
                                    reply.setRepliedTo(messageNotRepliedYet);
                                    if (srv.replyMessage(reply))
                                        System.out.println("Replied message!");
                                    else
                                        System.out.println("Couldn't send message");
                                } else {
                                    MessageDTO originalMessageDTO = messagesNotRepliedYet.get(index);
                                                if (srv.replyAll(originalMessageDTO,username,msgReply))
                                                    System.out.println("Replied to all!");
                                    else System.out.println("Failed to finish task!");

                                }
                            } else
                                keepGoing = false;
                        } else {
                            System.out.println("You didn't receive any new messages");
                            keepGoing = false;
                        }
                    }
                    System.out.println("Done!");
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }


    }
    public void sendMessage(){
            System.out.print("Enter userID");
            String from = input.nextLine();
            System.out.println("Enter users you wish to send message:");
            List<String> usersID = Arrays.asList(input.nextLine().split(" "));
            System.out.print("Your message:");
            String message = input.nextLine();
            List<UserDto<String>> recipients = usersID.stream().map(userName -> new UserDto<String>(userName,"","")).collect(Collectors.toList());
            MessageDTO messageDTO = new MessageDTO(new UserDto<String>(from,"",""),recipients,message,LocalDateTime.now(),0L);
            srv.sendMessage(messageDTO);
        System.out.println("Message sent!");
    }

    private void sendFriendshipRequest() {
        System.out.print("Enter userID:");
        String fromID = input.nextLine();
        System.out.print("Enter userID:");
        String toID = input.nextLine();
        try {
            this.srv.sendFriendshipRequest(new FriendshipRequestDTO<String>(new UserDto<String>(fromID,"","")
            ,new UserDto<String>(toID,"","")
            ,FriendshipRequestStatus.PENDING, LocalDate.now()));
            System.out.println("The request is now pending");
        }catch (InsufficientDataToExecuteTaskException | RepoError e) {
            System.out.println(e.getMessage());
        }

    }


    private void acceptOrRejectRequest() {
        System.out.print("Who send the friend request:");
        String sender = input.nextLine();
        System.out.print("Who is going to accept or decline the friend request:");
        String receiver = input.nextLine();
        System.out.println("1 for Accept\n2 for Decline\n");

        Integer selection;
        System.out.print("Your option:");
        Scanner input = new Scanner(System.in);
        selection = input.nextInt();
        try {
            switch (selection) {
                case 1:
                    this.srv.updateFriendshipRequestStatus(new FriendshipRequestDTO<String>(new UserDto<String>(sender,"","")
                            ,new UserDto<String>(receiver,"","")
                            ,FriendshipRequestStatus.APPROVED,LocalDate.now()));
                    System.out.println("The request's status is \"APPROVED\"!");
                    break;
                case 2:
                    this.srv.updateFriendshipRequestStatus(new FriendshipRequestDTO<String>(new UserDto<String>(sender,"","")
                            ,new UserDto<String>(receiver,"","")
                            ,FriendshipRequestStatus.REJECTED,LocalDate.now()));
                    System.out.println("The request's status is \"REJECTED\"!");
                    break;
            }
        }catch (InsufficientDataToExecuteTaskException | RepoError e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * menu
     */
    private  void menu() {
        /***************************************************/

        System.out.println("Choose from these choices");
        System.out.println("-------------------------\n");
        System.out.println("0  - Quit");
        System.out.println("1  - Add user");
        System.out.println("2  - Delete user");
        System.out.println("3  - Add friendship");
        System.out.println("4  - Remove friendship");
        System.out.println("5  - Count communities");
        System.out.println("6  - Friendliest community");
        System.out.println("7  - Get all users");
        System.out.println("9  - Print menu");
        System.out.println("10 - Get all user's friends by userID");
        System.out.println("11 - Get all user's friends by userID and date");
        System.out.println("12 - View conversation history");
        System.out.println("13 - ReplyMessages");
        System.out.println("14 - SendMessage");
        System.out.println("15 - Send friendship request");
        System.out.println("17 - Accept or reject a friendship request");
    }

    /**
     * console run
     */
    public void run(){
        boolean go=true;
        menu();
        while(go){

            Integer selection;
            System.out.print("Your option:");
            Scanner input = new Scanner(System.in);
            selection = input.nextInt();

            switch (selection){
                case 0:
                    go=false;
                    this.input.close();
                    break;
                case 1:
                    this.addUser();
                    break;
                case 2:
                    this.removeUser();
                    break;
                case 3:
                    this.addFriend();
                    break;
                case 4:
                    this.removeFriend();
                    break;
                case 5:
                    this.countCommunities();
                    break;
                case 6:
                    this.friendliestCommunity();
                    break;
                case 7:
                    this.getAllUsers();
                    break;
                case 9:
                    this.menu();
                    break;
                case 10:
                    this.getUserFriendships();
                    break;
                case 11:
                    this.getUserAllFriendsByDate();
                    break;
                case 12:
                    this.conversationHistory();
                    break;
                case 13:
                    this.replyMessage();
                    break;
                case 14:
                    sendMessage();
                    break;
                case 15:
                    sendFriendshipRequest();
                    break;
                case 17:
                    acceptOrRejectRequest();
                    break;
                default:
                    break;
            }
        }
        System.out.println("Done\n");
    }
}
