package ro.ubbcluj.map;

import ro.ubbcluj.map.Service.*;
import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.Tuple;
import ro.ubbcluj.map.model.ApplicationUser;
import ro.ubbcluj.map.model.*;

import ro.ubbcluj.map.myException.InsufficientDataToExecuteTaskException;
import ro.ubbcluj.map.myException.RepoError;
import ro.ubbcluj.map.repository.AppUserRepository;
import ro.ubbcluj.map.repository.FriendshipRequestRepository;
import ro.ubbcluj.map.repository.MessageRepository;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.dbo.*;
import ro.ubbcluj.map.repository.paging.PagedAppUserRepository;
import ro.ubbcluj.map.repository.paging.PagedFriendshipRepository;
import ro.ubbcluj.map.ui.Console;
import ro.ubbcluj.model.validators.FriendshipTupleIdValidator;
import ro.ubbcluj.model.validators.MessagesValidator;
import ro.ubbcluj.model.validators.UserStringIdValidator;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;


public class Main {


    public static void main(String[] args) {

        String url = "jdbc:postgresql://localhost:5432/SocialNetwork";
        String username = "postgres";
        String password = "polopolo123";
        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            PagedAppUserRepository<String, ApplicationUser> repoUser = new UserRepoDboPaginated(connection);
            PagedFriendshipRepository<Tuple<String, String>, Friendship> repoFriendship = new FriendshipRepoDboPaginated(connection);
            FriendshipService serviceFriendship = new FriendshipService(repoFriendship);
            UserService serviceUser = new UserService(repoUser);
            MessageRepository<Long, Message> messageRepository = new MessageRepoDbo(connection);
            FriendshipRequestRepository<Long, FriendshipRequest> RepoFriendshipRequest = new FriendshipRequestsDbo(connection);
            NetworkService service = new NetworkService(serviceFriendship, new FriendshipTupleIdValidator(), serviceUser, new UserStringIdValidator(), messageRepository, new MessagesValidator(), RepoFriendshipRequest, new ConversationDbo(connection));
            System.out.println(service.getConversationHistoryByDate("ms11@outlook.com",
                                                                "if@gmail.com",
                                                            LocalDateTime.of(2022,1,1,0,0),
                                                        LocalDateTime.of(2022,2,1,0,0)));
            /*Console console = new Console(service);
            console.run();*/
            EventsRepoDbo eventsRepoDbo = new EventsRepoDbo(connection);
            //System.out.println(eventsRepoDbo.findAllInASpecificMonthAndYear(LocalDate.of(2022,1,5)));
            //eventsRepoDbo.addInterestedUser("ana@wrernauer.com",new Event("",null,null,3l));
            //eventsRepoDbo.removeUninterestedUser("lmin@gmail.com",new Event("",null,null,3l));
           // eventsRepoDbo.saveEvent(new Event("Petrecere astazi",LocalDate.now(),null,0l));
           // System.out.println(eventsRepoDbo.findAllInASpecifiedDay(LocalDate.of(2022,1,5)));
            //System.out.println(eventsRepoDbo.getUpcomingEventsForOneUser(new ApplicationUser(null,null,"ana@wrernauer.com")));

        }catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

