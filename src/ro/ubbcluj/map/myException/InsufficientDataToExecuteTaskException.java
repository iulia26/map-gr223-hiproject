package ro.ubbcluj.map.myException;

public class InsufficientDataToExecuteTaskException extends Exception{
    public InsufficientDataToExecuteTaskException(String message) {
        super(message);
    }
}
