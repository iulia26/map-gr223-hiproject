package ro.ubbcluj.map.myException;

public class IDisAlreadyTakenException extends Exception{
    public IDisAlreadyTakenException(String message) {
        super(message);
    }
}
