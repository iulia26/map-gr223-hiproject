package ro.ubbcluj.map.myException;

public class RepoError extends Exception{
    public RepoError(String message) {
        super(message);
    }
}

